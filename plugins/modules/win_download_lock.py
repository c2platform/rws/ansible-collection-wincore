#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright: (c) 2024, Onno van der Straaten <@onknows>
# MIT License

DOCUMENTATION = r"""
---
module: win_download_lock
short_description: Creates a lock file to prevent concurrent downloads
description:
  - This module is used to create a lock file on Windows hosts to prevent multiple nodes from trying to download and process the same file simultaneously when the download is directed to a shared storage. This is particularly useful when using Ansible to target multiple Windows hosts.
  - It is used by the c2platform.wincore.download role, a generic role that can be included in other roles using `include_role`.
options:
  download:
    description:
      - A dictionary containing download-related parameters, including the URL of the file to download.
      - The only required key in this dictionary is 'url', but it can also contain 'unzip' to indicate whether the downloaded file should be unzipped.
    type: dict
    required: yes
  path:
    description:
      - The path where the lock file and the downloaded file will be placed. This should be a shared storage location accessible by all nodes.
    type: str
    required: yes
  lock_key:
    description:
      - Optional. A key that can be used to identify the lock owner. If provided and a lock file exists with key, the lock is considered to be owned by the current Ansible task.
    type: str
    required: no

author:
  - Onno van der Straaten (@onknows)
"""

EXAMPLES = r"""
- name: Create a download lock for a file
  win_download_lock:
    download:
      url: https://example.com/file.zip
      unzip: yes
    path: C:\shared\downloads

- name: Create a download lock without unzipping the file
  win_download_lock:
    download:
      url: https://example.com/file2.zip
    path: C:\shared\downloads
"""

RETURN = r"""
unzip:
  description: Indicates if the downloaded file needs to be unzipped.
  returned: always
  type: bool
  sample: false

download:
  description: Indicates if the file needs to be downloaded.
  returned: always
  type: bool
  sample: true

download_exists:
  description: Indicates if the download file already exists.
  returned: always
  type: bool
  sample: false

changed:
  description: Indicates that a lock file was created.
  returned: always
  type: bool
  sample: true

unzip_path_exists:
  description: Indicates if the unzip path already exists.
  returned: always
  type: bool
  sample: false

lock_file:
  description: The full path of the lock file.
  returned: always
  type: str
  sample: C:\\shared\\downloads\\file.zip.lock

download_file:
  description: The full path to the download file.
  returned: always
  type: str
  sample: C:\\shared\\downloads\\file.zip

unzip_path:
  description: The full path to the directory where the file will be unzipped.
  returned: when unzip is true
  type: str
  sample: C:\\shared\\downloads\\file
"""
