#!/usr/bin/python
# -*- coding: utf-8 -*-

DOCUMENTATION = r"""
---
module: win_pip
short_description: Manages Python packages with pip on Windows
description:
  - Installs, uninstalls, and upgrades Python packages using the pip package manager.
options:
  name:
    description:
      - Name of the Python package to manage.
    required: true
    type: str
  version:
    description:
      - The specific version of the package to be installed or the condition to meet.
      - If not specified, the latest version will be installed when using C(state=present) without C(latest).
    type: str
  state:
    description:
      - Desired state of the package.
    type: str
    choices: [ absent, present, latest ]
    default: present
  executable:
    description:
      - The path to the pip executable.
      - If not specified, the module will use the pip executable in the current environment.
    type: str
  extra_args:
    description:
      - Additional arguments to pass to pip.
    type: str
requirements:
  - Python
  - pip
author:
  - "Onno van der Straaten (@onknows)"
notes:
  - This module works with Windows and requires PowerShell.
  - The module can run in check mode.
"""

EXAMPLES = r"""
- name: Install a specific version of a package
  win_pip:
    name: somepackage
    version: "1.2.3"
    state: present

- name: Upgrade a package to the latest version
  win_pip:
    name: somepackage
    state: latest

- name: Uninstall a package
  win_pip:
    name: somepackage
    state: absent

- name: Install a package with extra arguments
  win_pip:
    name: somepackage
    extra_args: "--no-cache-dir --force-reinstall"
"""

RETURN = r"""
cmd:
  description: The pip command executed by the module.
  returned: always
  type: str
run_command:
  description: The result from running the pip command.
  returned: when command is run
  type: complex
  contains:
    stdout:
      description: The standard output from running the command.
      type: str
    stderr:
      description: The standard error output from running the command.
      type: str
    rc:
      description: The return code from running the command.
      type: int
package_installed:
  description: Indicates if the package is installed.
  returned: always
  type: bool
package_version_installed:
  description: Indicates if the version of the package is installed.
  returned: always
  type: bool
packages:
  description: A list of dict objects, each containing the name and version of a package.
  returned: always
  type: list
  elements: dict
  contains:
    name:
      description: The name of the package.
      type: str
    version:
      description: The installed version of the package.
      type: str
"""
