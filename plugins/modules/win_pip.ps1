#Requires -Module Ansible.ModuleUtils.Legacy
#Requires -Module Ansible.ModuleUtils.CommandUtil
#AnsibleRequires -OSVersion 6.2
#AnsibleRequires -CSharpUtil Ansible.Basic

$spec = @{
    options = @{
        name = @{ type = "str"; required = $true }
        version = @{ type = "str" }
        state = @{
            type = "str"
            default = "present"
            choices = @("absent", "present", "latest")
        }
        executable = @{ type = "str" }
        extra_args = @{ type = "str" }
    }
    supports_check_mode = $true
}

$module = [Ansible.Basic.AnsibleModule]::Create($args, $spec)

$ErrorActionPreference = "Stop"
Set-StrictMode -Version 2.0

$name = $module.Params.name
$version = $module.Params.version
$state = $module.Params.state
$extraArgs = $module.Params.extra_args

# Initialize variables to track installation and version status
$packageInstalled = $false
$packageVersionInstalled = $false

# Get the current list of installed packages using pip
$pipList = Run-Command -command "pip list"

# Initialize an array to hold the package details
$packages = @()

# Skip the header line and then process each line of the pip list output
$pipList.stdout -split '\r?\n' | Select-Object -Skip 2 | ForEach-Object {
    # Match lines where the package name is followed by its version, accounting for variable amounts of whitespace
    if ($_ -match "^\s*(\S+)\s+(\S+)$") {
        # Create a custom object with the package name and version
        $package = New-Object PSObject -Property @{
            Name = $matches[1]
            Version = $matches[2]
        }
        # Add the package object to the array
        $packages += $package
    }
}

# Check each package to see if it matches the desired name and version
foreach ($package in $packages) {
    if ($package.Name -eq $name) {
        $packageInstalled = $true

        if ($package.Version -eq $version) {
            $packageVersionInstalled = $true
            break # Exit the loop early since both conditions are met
        }
    }
}

$pipArgs = @()
switch ($state) {
    "present" {
        $pipArgs += "install"
        if ($version) {
            $name = $name | ForEach-Object { "$_==$version" }
        }
        $pipArgs += $name
    }
    "absent" {
        $pipArgs += "uninstall", "-y", $name
    }
    "latest" {
        $pipArgs += "install", "-U", $name
    }
}

if ($extraArgs) {
    $pipArgs += $extraArgs -split ' '
}

$pipCmd = "pip $pipArgs"
if ($state -eq 'absent' -and $packageInstalled) {
    $result = Run-Command -command $pipCmd
    $module.Result.changed = $true
} elseif (-not $packageInstalled -or -not $packageVersionInstalled -and $state -eq 'present') {
    $result = Run-Command -command $pipCmd
    $module.Result.changed = $true
} else {
    $result = "No changes required."
    $module.Result.changed = $false
}
if ($module.Result.changed) {
    if ($result.rc -ne 0) {
        $module.FailJson($result.stderr)
    }
}
$module.Result.cmd = $pipCmd
$module.Result.run_command = $result
$module.Result.packageInstalled = $packageInstalled
$module.Result.packageVersionInstalled = $packageVersionInstalled
$module.Result.packages = $packages

$module.ExitJson()
