#!/usr/bin/python
# -*- coding: utf-8 -*-

DOCUMENTATION = r"""
---
module: win_certificate
short_description: Manages certificates in Windows
description:
  - This module allows managing certificates in Windows.
  - You can add or remove certificates using this module.
version_added: "1.0.0"
requirements:
  - Windows OS version 6.2 or newer.
options:
  dns_name:
    description:
      - Specifies the DNS name for the certificate.
    type: str
    required: true
  validity_days:
    description:
      - Defines the validity period of the certificate in days.
    type: int
    default: 365
    required: false
  store_location_my:
    description:
      - Specifies the store location for certificate creation.
      - Default is 'Cert:\LocalMachine\My'.
    type: str
    default: 'Cert:\LocalMachine\My'
    required: false
  store_location:
    description:
      - The target store location for the certificate to be finally imported or checked.
      - Default is the same as `store_location_my`.
    type: str
    default: 'Cert:\LocalMachine\My'
    required: false
  state:
    description:
      - Specifies the desired state of the certificate.
    type: str
    choices: [ 'present', 'absent' ]
    default: 'present'
  tmp_file_path:
    description:
      - Temporary file path for exporting the certificate if needed.
      - Default is 'C:\Temp\ExportedCertificate.cer'.
    type: str
    default: 'C:\Temp\ExportedCertificate.cer'
    required: false
  friendly_name:
    description:
      - The friendly name of the certificate, used to easily identify the certificate in the certificate store.
      - Default is 'AnsibleCert'.
    type: str
    default: 'AnsibleCert'
    required: false

author:
  - onknows
"""

EXAMPLES = r"""
- name: Manage certificate with a friendly name
  c2platform.wincore.win_certificate:
    dns_name: "{{ ansible_hostname }}"
    validity_days: 3650
    state: present
    friendly_name: "MyWebServerCert"
  register: _cert

- name: Add a HTTPS binding using the newly created certificate
  community.windows.win_iis_webbinding:
    name: "{{ arcgis_webadaptor_ssl_certificate_iis_web_site_name }}"
    protocol: https
    port: 443
    ip: '*'
    certificate_hash: "{{ _cert.thumbPrint }}"
    state: present
  when: '"thumbPrint" in _cert'
"""

RETURN = r"""
msg:
  description: A message describing the result of the operation.
  returned: always
  type: str
  sample: 'Certificate www.example.com created and imported to Cert:\LocalMachine\My'
thumbPrint:
  description: The thumbprint of the managed certificate.
  returned: when the certificate is present or created
  type: str
  sample: 'ABCDEF1234567890ABCDEF1234567890ABCDEF12'
"""
