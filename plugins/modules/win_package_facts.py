#!/usr/bin/python
# -*- coding: utf-8 -*-

DOCUMENTATION = r"""
---
module: win_package_facts
short_description: Gather facts about installed packages on Windows hosts
description:
  - This module gathers facts about the packages that are installed on a Windows host.
  - The facts will include a list of all installed packages.
version_added: "1.0.0"
requirements:
  - Windows OS version 6.2 or newer.
options: {}
author:
  - onknows
"""

EXAMPLES = r"""
- name: Get Installed packages
  c2platform.wincore.win_package_facts:

- name: Display Installed Packages
  debug:
    var: ansible_facts.installed_packages

- name: Install WebDeploy for IIS
  ansible.windows.win_package:
    path: C:/temp/WebDeploy_amd64_en-US.msi
    arguments: 'ADDLOCAL=ALL LicenseAccepted="0"'
    state: present
  when: '"Microsoft Web Deploy 3.6" not in ansible_facts.installed_packages'
"""

RETURN = r"""
ansible_facts:
  description: Facts to add to ansible_facts.
  returned: always
  type: complex
  contains:
    installed_packages:
      description: A list of names of the installed packages.
      type: list
      elements: str
      sample: ['Git version 2.29.2.windows.2', 'VLC media player']
msg:
  description: Summary message about the fact gathering operation.
  type: str
  returned: always
  sample: 'Package facts gathered'
"""
