#!powershell

#Requires -Module Ansible.ModuleUtils.Legacy
#AnsibleRequires -OSVersion 6.2
#AnsibleRequires -CSharpUtil Ansible.Basic

$spec = @{
    options = @{
        download = @{type = "dict"; required = $true }
        path = @{ type = "str"; required = $true }
        lock_key = @{ type = "str"; required = $false }
    }
}

$module = [Ansible.Basic.AnsibleModule]::Create($args, $spec)
$ErrorActionPreference = "Stop"
Set-StrictMode -Version 2.0

function Get-UrlSha256 {
    param (
        [String]$Url
    )

    $sha256 = New-Object -TypeName System.Security.Cryptography.SHA256CryptoServiceProvider
    $utf8 = New-Object -TypeName System.Text.UTF8Encoding
    $bytes = $utf8.GetBytes($Url)
    $hash = $sha256.ComputeHash($bytes)
    return [BitConverter]::ToString($hash).Replace("-", "").ToLower()
}

function Create-LockFile {
    param (
        [String]$LockFilePath,
        [String]$LockKey, # Added parameter for hostname
        [Int]$RetryCount = 2,
        [Int]$RetryInterval = 3
    )

    $tryCount = 0
    $lockFileCreated = $false
    while ($tryCount -lt $RetryCount -and -not $lockFileCreated) {
        if (Test-Path -Path $LockFilePath) {
            $currentHostName = Get-Content -Path $LockFilePath -ErrorAction SilentlyContinue
            if ($currentHostName -eq $LockKey) {
                $module.Result.msg = "Lock file already exists with matching lock key. Continuing download."
                $module.Result.download = $true
                $lockFileCreated = $true
                return $true # Continue processing as if lock file was created
            } else {
                $module.Result.msg = "Lock file already exists with a different lock key. Download might be in progress."
                $module.Result.download = $false
                return $false
            }
        } else {
            try {
                $null = New-Item -Path $LockFilePath -ItemType File -Value "$LockKey"  # without quotes → out of memory error
                $module.Result.download = $true
                $lockFileCreated = $true
            } catch {
                $module.Result.msg = "Failed to create the lock file on attempt $tryCount due to error: $_"
                Start-Sleep -Seconds $RetryInterval
            }
        }
        $tryCount++
    }

    if (-not $lockFileCreated) {
        $module.Result.download = $false
        $module.FailJson($module.Result.msg)
    }

    return $lockFileCreated
}

$url = $module.Params.download.url
$path = $module.Params.path
$unzip = if ($module.Params.download.ContainsKey('unzip')) { $module.Params.download.unzip } else { $false }
$lockKey = $module.Params.lock_key

$urlSha256 = Get-UrlSha256 -Url $url
$lockFile = Join-Path -Path $path -ChildPath "$urlSha256.lock"
$fileName = [System.IO.Path]::GetFileName($url)
$directoryName = [System.IO.Path]::GetFileNameWithoutExtension($fileName)
$directoryPath = Join-Path -Path $path -ChildPath $directoryName
$downloadFile = Join-Path -Path $path -ChildPath $fileName

$module.Result.unzip = $false
$module.Result.download = $false
$module.Result.download_exists = $false
$module.Result.changed = $false
$module.Result.unzip_path_exists = $false
$module.Result.lock_file = $lockFile
$module.Result.download_file = $downloadFile
$module.Result.unzip_path =$directoryPath

if (Test-Path -Path $downloadFile) {
    $module.Result.download_exists = $true
}
if (Test-Path -Path $directoryPath) {
    $module.Result.unzip_path_exists = $true
}

if ( -not $module.Result.download_exists -or (-not $module.Result.unzip_path_exists -and $unzip) )
{
    if (Create-LockFile -LockFilePath $lockFile -LockKey $lockKey -RetryCount 5 -RetryInterval 3) {
        # This node will perform the download and / or unzip
        $module.Result.msg = "Lock file created, download and/or unzip can proceed."
        if ($unzip -and -not $module.Result.unzip_path_exists) {
            $module.Result.unzip = $true
        }
        if (-not $module.Result.download_exists) {
            $module.Result.download = $true
        }
        $module.Result.changed = $true
    }
}

$module.ExitJson()
