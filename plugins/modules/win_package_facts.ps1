#!powershell

#Requires -Module Ansible.ModuleUtils.Legacy
#AnsibleRequires -OSVersion 6.2
#AnsibleRequires -CSharpUtil Ansible.Basic

$spec = @{}

$module = [Ansible.Basic.AnsibleModule]::Create($args, $spec)
$ErrorActionPreference = "Stop"
Set-StrictMode -Version 2.0

$installedPackages = Get-Package | Select-Object -ExpandProperty Name

$module.Result.msg = "Package facts gathered"
$module.Result.changed = $false

$ansible_facts = @{
    installed_packages = $installedPackages
}

$module.Result.ansible_facts = $ansible_facts

# $ansible_facts | ConvertTo-Json

$module.ExitJson()
