from __future__ import absolute_import, division, print_function

__metaclass__ = type

from ansible.errors import AnsibleFilterError


def win_iis_webbinding_cert_hash(item, certificates):
    if "certificate_hash" in item:
        return item["certificate_hash"]
    elif "certificate_friendly_name" in item:
        for cert in certificates:
            if cert.get("friendly_name") == item["certificate_friendly_name"]:
                return cert.get("thumbprint")
        return "__OMIT_PLACEHOLDER__"
    return "__OMIT_PLACEHOLDER__"


class FilterModule(object):
    def filters(self):
        return {
            "win_iis_webbinding_cert_hash": win_iis_webbinding_cert_hash,
        }
