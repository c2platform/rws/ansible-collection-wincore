from __future__ import absolute_import, division, print_function

__metaclass__ = type

from ansible.errors import AnsibleFilterError


def download_extract_folder(url, download_temp_dir_path=None):
    import os
    from urllib.parse import urlparse

    # Get the basename of the file from the URL without the extension
    folder_name = os.path.splitext(os.path.basename(urlparse(url).path))[0]

    if download_temp_dir_path:
        result_path = "{0}\\{1}".format(download_temp_dir_path, folder_name)
    else:
        result_path = folder_name
    return result_path


class FilterModule(object):
    """Ansible core jinja2 filters"""

    def filters(self):
        return {"download_extract_folder": download_extract_folder}
