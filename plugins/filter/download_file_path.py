from __future__ import absolute_import, division, print_function

__metaclass__ = type


from ansible.errors import AnsibleFilterError


def download_file_path(url, download_temp_dir_path=None):
    import os
    from urllib.parse import urlparse

    # Get the basename of the file from the URL
    file_basename = os.path.basename(urlparse(url).path)

    if download_temp_dir_path:
        result_path = "{0}/{1}".format(download_temp_dir_path, file_basename)
    else:
        result_path = file_basename
    return result_path


class FilterModule(object):
    """Ansible core jinja2 filters"""

    def filters(self):
        return {"download_file_path": download_file_path}
