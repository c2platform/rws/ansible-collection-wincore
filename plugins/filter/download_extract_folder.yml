---
DOCUMENTATION:
  name: c2platform.wincore.download_extract_folder
  author: onknows
  version_added: "1.0.0"
  short_description: >-
    Extracts the folder name intended for extraction from a given URL
  description: >-
    This filter takes a URL and an optional download directory path. It parses
    the URL to determine the folder name (derived from the file name in the URL)
    where contents are expected to be extracted. If a download directory path is
    provided, it appends the determined folder name to this path.
  options:
    url:
      description: >-
        The URL of the file that needs to be downloaded and extracted.
      required: True
      type: str
    download_temp_dir_path:
      description: >-
        The path to a temporary directory where the file should be downloaded.
      required: False
      type: str
  examples: |
    - name: Extract folder path from URL
      description: Determine the folder path for extraction based on the URL.
      code: |
        "{{ 'http://example.com/archive.zip' | download_extract_folder }}"
    - name: Extract folder path with custom download directory
      description: >-
        Determine the folder path for extraction with a
        specified download directory.
      code: |
        fme_flow_extraction_path: >-
          {{ fme_flow_versions[fme_flow_version]["url"]
          | download_extract_folder(fme_flow_temp_dir["path"]) }}
  returns:
    path:
      description: >-
        The full path where the contents of the URL should be extracted.
      type: str
      returned: always
