# Ansible Role c2platform.wincore.ad

The `c2platform.wincore.ad` Ansible role is designed to streamline the
management of Microsoft Active Directory. It leverages the Ansible modules
available in the Ansible collection
[`microsoft.ad`](https://docs.ansible.com/ansible/latest/collections/microsoft/ad/).
. This role is structured similarly to the Ansible role
[`c2platform.wincore.win`](https://gitlab.com/c2platform/rws/ansible-collection-wincore)
and utilizes both simple lists (`ad_domain`, `ad_domain_controller`) and a
versatile dictionary variable (`ad_resources`) for seamless configuration. It's
akin to the `win_resources` variable in the `c2platform.wincore.win` role. For
more information on using simple lists or dictionaries, please refer to the
`c2platform.wincore.win` role.


- [Requirements](#requirements)
- [Role Variables](#role-variables)
  - [`ad_resources_types`](#ad_resources_types)
  - [`ad_resources`](#ad_resources)
    - [Attribute `resources`](#attribute-resources)
  - [`ad_` lists and types](#ad_-lists-and-types)
  - [Configuring DNS Settings Using `dns_setting.ps.j2`](#configuring-dns-settings-using-dns_settingpsj2)
- [Dependencies](#dependencies)
- [Example Playbook](#example-playbook)

## Requirements

<!-- Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required. -->

## Role Variables

<!--  A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well. -->

### `ad_resources_types`

The `ad_resources_types` variable serves as a critical configuration element for
specifying the Ansible modules to be used. This versatile variable can take two
forms: a simple list or a dictionary, depending on your specific needs. The
[`c2platform.wincore.win`](https://gitlab.com/c2platform/rws/ansible-collection-wincore)
Ansible role features a similar variable called `win_resources_type`. Refer to
that role for more information.

### `ad_resources`

The `ad_resources` variable plays a pivotal role in defining tasks that involve
multiple Ansible-supported modules. The `c2platform.wincore.win` Ansible role
also has a similar variable named `ad_resources`. Check the documentation for
that role for more details.

#### Attribute `resources`

You can utilize a `resources` attribute to construct list items of
`ad_resources` as a list of lists of a similar `type`.

For example, to create two domain we can do that as follows:

```yaml
ad_resources:
  - name: user1
    type: ad_domain_user
    password: Supersecret!
    dns_domain_name: ad.c2platform.org
    domain_admin_user: user1@ad.c2platform.org
    domain_admin_password: Supersecret!
    state: present
  - name: user2
    type: ad_domain_user
    password: Supersecret!
    dns_domain_name: ad.c2platform.org
    domain_admin_user: user2@ad.c2platform.org
    domain_admin_password: Supersecret!
    state: present
```

Alternatively, you can use a `resources` attribute to rewrite this configuration
as follow:

```yaml
ad_resources:
  - name: User 1 & 2
    type: ad_user
    resources:
      -  name: user1
         password: Supersecret!
         dns_domain_name: ad.c2platform.org
         domain_admin_user: user1@ad.c2platform.org
         domain_admin_password: Supersecret!
         state: present
      -  name: user2
         password: Supersecret!
         dns_domain_name: ad.c2platform.org
         domain_admin_user: user2@ad.c2platform.org
         domain_admin_password: Supersecret!
         state: present
```

### `ad_` lists and types

| List                   | Type                   | Module                                                                                                                                                |
|------------------------|------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------|
| `ad_domain`            | `ad_domain`            | [`microsoft.ad.domain`](https://docs.ansible.com/ansible/latest/collections/microsoft/ad/domain_module.html)                                          |
| `ad_domain_controller` | `ad_domain_controller` | [`microsoft.ad.domain_controller`](https://docs.ansible.com/ansible/latest/collections/microsoft/ad/domain_controller_module.html)                    |
| `ad_user`              | `ad_user`              | [`microsoft.ad.user`](https://docs.ansible.com/ansible/latest/collections/microsoft/ad/user_module.html#ansible-collections-microsoft-ad-user-module) |
| `ad_membership`        | `ad_membership`        | [`microsoft.ad.membership`](https://docs.ansible.com/ansible/latest/collections/microsoft/ad/membership_module.html)                                  |
| `ad_dns_client`        | `ad_dns_client`        | [`ansible.windows.win_dns_client`](https://docs.ansible.com/ansible/latest/collections/ansible/windows/win_dns_client_module.html)                    |
| `ad_powershell`        | `ad_powershell`        | `ad_powershell` is the same as [`win_powershell`](../win/README.md#win_powershell)  |


### Configuring DNS Settings Using `dns_setting.ps.j2`

For a practical demonstration, please consult the `groups_vars/ad/main.yml` file
located within the
[RWS Inventory Project](https://c2platform.org/en/docs/gitlab/c2platform/rws/ansible-gis).
Within this file, there is a list variable `ad_resources`: that ensures the
Active Directory (AD) DNS server only listens on the IP address `1.1.8.108`,
excluding `10.0.2.15`:

```yaml
  - name: DNS should only listen on 1.1.8.108  # And not also on 10.0.2.15
    type: ad_powershell
    script: >-
      {{ lookup('ansible.builtin.template'
      , 'dns_setting.ps.j2'
      , template_vars={'dns_setting': gs_dns_setting }) }}
```

The variable `gs_dns_setting` is defined in the same file with the following
properties:

```yaml
gs_dns_setting:
  property: ListeningIPAddress
  value: ("1.1.8.108")
```

## Dependencies

<!--   A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles. -->

## Example Playbook

<!--   Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too: -->

To find practical examples demonstrating the utilization of the
`c2platform.wincore.ad` Ansible role, please consult the
`groups_vars/ad /main.yml` file located within the
[RWS Inventory Project](https://c2platform.org/en/docs/gitlab/c2platform/rws/ansible-gis).
This file provides insightful illustrations of how to employ the mentioned
Ansible role.
