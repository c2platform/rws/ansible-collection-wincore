# Ansible Role c2platform.wincore.win

Manage MS Windows systems using more than 117+ Ansible modules from the
`ansible.windows`, `community.windows` and `chocolatey.chocolatey` collection.
The purpose of this role is to allow you to manage Windows resources without any
Ansible engineering of Ansible collections or roles. Access to these modules is
gained through the use of a generic, flexible `win_resources` or `win_<module>`
variables where the "module" can be any of the supported Ansible modules for
example `win_firewall`. On top of that these variables can be simple lists but
also dictionaries. As dictionaries these variables can utilize flexible,
powerful configuration approaches with dictionary merging.

- [Requirements](#requirements)
- [Role Variables](#role-variables)
  - [`win_roles`](#win_roles)
  - [`win_<module>` and `win_resources_types`](#win_module-and-win_resources_types)
  - [`win_resources`](#win_resources)
  - [Understanding `type` vs `module` in `win_resources`](#understanding-type-vs-module-in-win_resources)
  - [`win_proxy_password`, `win_proxy_url` and `win_proxy_username`](#win_proxy_password-win_proxy_url-and-win_proxy_username)
  - [Leveraging `resources` and `defaults` for Efficiency](#leveraging-resources-and-defaults-for-efficiency)
  - [Using Dictionaries and Lists](#using-dictionaries-and-lists)
    - [Using Lists (Less Flexible)](#using-lists-less-flexible)
    - [Using Dictionaries (More Flexible)](#using-dictionaries-more-flexible)
  - [Modules](#modules)
    - [`fail`: Fail with custom message](#fail-fail-with-custom-message)
    - [`include_role`: Load and execute a role](#include_role-load-and-execute-a-role)
    - [`psexec`: Runs commands on a remote Windows host based on the PsExec model](#psexec-runs-commands-on-a-remote-windows-host-based-on-the-psexec-model)
    - [`slurp`: Slurps a file from remote nodes](#slurp-slurps-a-file-from-remote-nodes)
    - [`win_acl`: Set file/directory/registry permissions for a system user or group](#win_acl-set-filedirectoryregistry-permissions-for-a-system-user-or-group)
    - [`win_acl_inheritance`: Change ACL inheritance](#win_acl_inheritance-change-acl-inheritance)
    - [`win_audit_policy_system`: Used to make changes to the system wide Audit Policy](#win_audit_policy_system-used-to-make-changes-to-the-system-wide-audit-policy)
    - [`win_audit_rule`: Adds an audit rule to files, folders, or registry keys](#win_audit_rule-adds-an-audit-rule-to-files-folders-or-registry-keys)
    - [`win_auto_logon`: Adds or Sets auto logon registry keys.](#win_auto_logon-adds-or-sets-auto-logon-registry-keys)
    - [`win_certificate`: Manages certificates in Windows](#win_certificate-manages-certificates-in-windows)
    - [`win_certificate_info`: Get information on certificates from a Windows Certificate Store](#win_certificate_info-get-information-on-certificates-from-a-windows-certificate-store)
    - [`win_certificate_store`: Manages the certificate store](#win_certificate_store-manages-the-certificate-store)
    - [`win_chocolatey`: Manage packages using chocolatey](#win_chocolatey-manage-packages-using-chocolatey)
    - [`win_chocolatey_config`: Manages Chocolatey config settings](#win_chocolatey_config-manages-chocolatey-config-settings)
    - [`win_chocolatey_feature`: Manages Chocolatey features](#win_chocolatey_feature-manages-chocolatey-features)
    - [`win_chocolatey_source`: Manages Chocolatey sources](#win_chocolatey_source-manages-chocolatey-sources)
    - [`win_command`: Executes a command on a remote Windows node](#win_command-executes-a-command-on-a-remote-windows-node)
    - [`win_computer_description`: Set windows description, owner and organization](#win_computer_description-set-windows-description-owner-and-organization)
    - [`win_copy`: Copies files to remote locations on windows hosts](#win_copy-copies-files-to-remote-locations-on-windows-hosts)
    - [`win_credential`: Manages Windows Credentials in the Credential Manager](#win_credential-manages-windows-credentials-in-the-credential-manager)
    - [`win_data_deduplication`: Module to enable Data Deduplication on a volume.](#win_data_deduplication-module-to-enable-data-deduplication-on-a-volume)
    - [`win_defrag`: Consolidate fragmented files on local volumes](#win_defrag-consolidate-fragmented-files-on-local-volumes)
    - [`win_dhcp_lease`: Manage Windows Server DHCP Leases](#win_dhcp_lease-manage-windows-server-dhcp-leases)
    - [`win_disk_facts`: Show the attached disks and disk information of the target host](#win_disk_facts-show-the-attached-disks-and-disk-information-of-the-target-host)
    - [`win_disk_image`: Manage ISO/VHD/VHDX mounts on Windows hosts](#win_disk_image-manage-isovhdvhdx-mounts-on-windows-hosts)
    - [`win_dns_client`: Configures DNS lookup on Windows hosts](#win_dns_client-configures-dns-lookup-on-windows-hosts)
    - [`win_dns_record`: Manage Windows Server DNS records](#win_dns_record-manage-windows-server-dns-records)
    - [`win_dns_zone`: Manage Windows Server DNS Zones](#win_dns_zone-manage-windows-server-dns-zones)
    - [`win_dotnet_ngen`: Runs ngen to recompile DLLs after .NET  updates](#win_dotnet_ngen-runs-ngen-to-recompile-dlls-after-net--updates)
    - [`win_dsc`: Invokes a PowerShell DSC configuration](#win_dsc-invokes-a-powershell-dsc-configuration)
    - [`win_environment`: Modify environment variables on windows hosts](#win_environment-modify-environment-variables-on-windows-hosts)
    - [`win_eventlog`: Manage Windows event logs](#win_eventlog-manage-windows-event-logs)
    - [`win_eventlog_entry`: Write entries to Windows event logs](#win_eventlog_entry-write-entries-to-windows-event-logs)
    - [`win_feature`: Installs and uninstalls Windows Features on Windows Server](#win_feature-installs-and-uninstalls-windows-features-on-windows-server)
    - [`win_feature_info`: Gather information about Windows features](#win_feature_info-gather-information-about-windows-features)
    - [`win_file`: Creates, touches or removes files or directories](#win_file-creates-touches-or-removes-files-or-directories)
    - [`win_file_compression`: Alters the compression of files and directories on NTFS partitions.](#win_file_compression-alters-the-compression-of-files-and-directories-on-ntfs-partitions)
    - [`win_file_version`: Get DLL or EXE file build version](#win_file_version-get-dll-or-exe-file-build-version)
    - [`win_firewall`: Enable or disable the Windows Firewall](#win_firewall-enable-or-disable-the-windows-firewall)
    - [`win_firewall_rule`: Windows firewall automation](#win_firewall_rule-windows-firewall-automation)
    - [`win_format`: Formats an existing volume or a new volume on an existing partition on Windows](#win_format-formats-an-existing-volume-or-a-new-volume-on-an-existing-partition-on-windows)
    - [`win_get_url`: Downloads file from HTTP, HTTPS, or FTP to node](#win_get_url-downloads-file-from-http-https-or-ftp-to-node)
    - [`win_group`: Add and remove local groups](#win_group-add-and-remove-local-groups)
    - [`win_group_membership`: Manage Windows local group membership](#win_group_membership-manage-windows-local-group-membership)
    - [`win_hostname`: Manages local Windows computer name](#win_hostname-manages-local-windows-computer-name)
    - [`win_hosts`: Manages hosts file entries on Windows.](#win_hosts-manages-hosts-file-entries-on-windows)
    - [`win_hotfix`: Install and uninstalls Windows hotfixes](#win_hotfix-install-and-uninstalls-windows-hotfixes)
    - [`win_http_proxy`: Manages proxy settings for WinHTTP](#win_http_proxy-manages-proxy-settings-for-winhttp)
    - [`win_iis_virtualdirectory`: Configures a virtual directory in IIS](#win_iis_virtualdirectory-configures-a-virtual-directory-in-iis)
    - [`win_iis_webapplication`: Configures IIS web applications](#win_iis_webapplication-configures-iis-web-applications)
    - [`win_iis_webapppool`: Configure IIS Web Application Pools](#win_iis_webapppool-configure-iis-web-application-pools)
    - [`win_iis_webbinding`: Configuring IIS Web Bindings](#win_iis_webbinding-configuring-iis-web-bindings)
    - [`win_iis_website`: Configures a IIS Web site](#win_iis_website-configures-a-iis-web-site)
    - [`win_inet_proxy`: Manages proxy settings for WinINet and Internet Explorer](#win_inet_proxy-manages-proxy-settings-for-wininet-and-internet-explorer)
    - [`win_initialize_disk`: Initializes disks on Windows Server](#win_initialize_disk-initializes-disks-on-windows-server)
    - [`win_lineinfile`: Ensure a particular line is in a file, or replace an existing line using a back-referenced regular expression](#win_lineinfile-ensure-a-particular-line-is-in-a-file-or-replace-an-existing-line-using-a-back-referenced-regular-expression)
    - [`win_listen_ports_facts`: Recopilates the facts of the listening ports of the machine](#win_listen_ports_facts-recopilates-the-facts-of-the-listening-ports-of-the-machine)
    - [`win_mapped_drive`: Map network drives for users](#win_mapped_drive-map-network-drives-for-users)
    - [`win_msg`: Sends a message to logged in users on Windows hosts](#win_msg-sends-a-message-to-logged-in-users-on-windows-hosts)
    - [`win_net_adapter_feature`: Enable or disable certain network adapters.](#win_net_adapter_feature-enable-or-disable-certain-network-adapters)
    - [`win_netbios`: Manage NetBIOS over TCP/IP settings on Windows.](#win_netbios-manage-netbios-over-tcpip-settings-on-windows)
    - [`win_nssm`: Install a service using NSSM](#win_nssm-install-a-service-using-nssm)
    - [`win_optional_feature`: Manage optional Windows features](#win_optional_feature-manage-optional-windows-features)
    - [`win_owner`: Set owner](#win_owner-set-owner)
    - [`win_package`: Installs/uninstalls an installable package](#win_package-installsuninstalls-an-installable-package)
    - [`win_pagefile`: Query or change pagefile configuration](#win_pagefile-query-or-change-pagefile-configuration)
    - [`win_partition`: Creates, changes and removes partitions on Windows Server](#win_partition-creates-changes-and-removes-partitions-on-windows-server)
    - [`win_path`: Manage Windows path environment variables](#win_path-manage-windows-path-environment-variables)
    - [`win_pester`: Run Pester tests on Windows hosts](#win_pester-run-pester-tests-on-windows-hosts)
    - [`win_ping`: A windows version of the classic ping module](#win_ping-a-windows-version-of-the-classic-ping-module)
    - [`win_pip`: Manages Python packages with pip on Windows](#win_pip-manages-python-packages-with-pip-on-windows)
    - [`win_power_plan`: Changes the power plan of a Windows system](#win_power_plan-changes-the-power-plan-of-a-windows-system)
    - [`win_powershell`: Run PowerShell scripts](#win_powershell-run-powershell-scripts)
    - [Install Package Provider using template `install_package_provider.ps.j2`](#install-package-provider-using-template-install_package_providerpsj2)
    - [Change Network Adapter Properties using template `net_adapter.ps.j2`.](#change-network-adapter-properties-using-template-net_adapterpsj2)
    - [`win_psmodule`: Adds or removes a Windows PowerShell module](#win_psmodule-adds-or-removes-a-windows-powershell-module)
    - [`win_psmodule_info`: Gather information about PowerShell Modules](#win_psmodule_info-gather-information-about-powershell-modules)
    - [`win_psrepository`: Adds, removes or updates a Windows PowerShell repository.](#win_psrepository-adds-removes-or-updates-a-windows-powershell-repository)
    - [`win_psrepository_copy`: Copies registered PSRepositories to other user profiles](#win_psrepository_copy-copies-registered-psrepositories-to-other-user-profiles)
    - [`win_psrepository_info`: Gather information about PSRepositories](#win_psrepository_info-gather-information-about-psrepositories)
    - [`win_psscript`: Install and manage PowerShell scripts from a PSRepository](#win_psscript-install-and-manage-powershell-scripts-from-a-psrepository)
    - [`win_psscript_info`: Gather information about installed PowerShell Scripts](#win_psscript_info-gather-information-about-installed-powershell-scripts)
    - [`win_pssession_configuration`: Manage PSSession Configurations](#win_pssession_configuration-manage-pssession-configurations)
    - [`win_rds_cap`: Manage Connection Authorization Policies (CAP) on a Remote Desktop Gateway server](#win_rds_cap-manage-connection-authorization-policies-cap-on-a-remote-desktop-gateway-server)
    - [`win_rds_rap`: Manage Resource Authorization Policies (RAP) on a Remote Desktop Gateway server](#win_rds_rap-manage-resource-authorization-policies-rap-on-a-remote-desktop-gateway-server)
    - [`win_rds_settings`: Manage main settings of a Remote Desktop Gateway server](#win_rds_settings-manage-main-settings-of-a-remote-desktop-gateway-server)
    - [`win_reboot`: Reboot a windows machine](#win_reboot-reboot-a-windows-machine)
    - [`win_reg_stat`: Get information about Windows registry keys](#win_reg_stat-get-information-about-windows-registry-keys)
    - [`win_regedit`: Add, change, or remove registry keys and values](#win_regedit-add-change-or-remove-registry-keys-and-values)
    - [`win_region`: Set the region and format settings](#win_region-set-the-region-and-format-settings)
    - [`win_regmerge`: Merges the contents of a registry file into the Windows registry](#win_regmerge-merges-the-contents-of-a-registry-file-into-the-windows-registry)
    - [`win_robocopy`: Synchronizes the contents of two directories using Robocopy](#win_robocopy-synchronizes-the-contents-of-two-directories-using-robocopy)
    - [`win_route`: Add or remove a static route](#win_route-add-or-remove-a-static-route)
    - [`win_say`: Text to speech module for Windows to speak messages and optionally play sounds](#win_say-text-to-speech-module-for-windows-to-speak-messages-and-optionally-play-sounds)
    - [`win_scheduled_task`: Manage scheduled tasks](#win_scheduled_task-manage-scheduled-tasks)
    - [`win_scheduled_task_stat`: Get information about Windows Scheduled Tasks](#win_scheduled_task_stat-get-information-about-windows-scheduled-tasks)
    - [`win_scoop`: Manage packages using Scoop](#win_scoop-manage-packages-using-scoop)
    - [`win_scoop_bucket`: Manage Scoop buckets](#win_scoop_bucket-manage-scoop-buckets)
    - [`win_security_policy`: Change local security policy settings](#win_security_policy-change-local-security-policy-settings)
    - [`win_service`: Manage and query Windows services](#win_service-manage-and-query-windows-services)
    - [`win_service_info`: Gather information about Windows services](#win_service_info-gather-information-about-windows-services)
    - [`win_share`: Manage Windows shares](#win_share-manage-windows-shares)
    - [`win_shell`: Execute shell commands on target hosts](#win_shell-execute-shell-commands-on-target-hosts)
    - [`win_shortcut`: Manage shortcuts on Windows](#win_shortcut-manage-shortcuts-on-windows)
    - [`win_snmp`: Configures the Windows SNMP service](#win_snmp-configures-the-windows-snmp-service)
    - [`win_tempfile`: Creates temporary files and directories](#win_tempfile-creates-temporary-files-and-directories)
    - [`win_template`: Template a file out to a remote server](#win_template-template-a-file-out-to-a-remote-server)
    - [`win_timezone`: Sets Windows machine timezone](#win_timezone-sets-windows-machine-timezone)
    - [`win_toast`: Sends Toast windows notification to logged in users on Windows 10 or later hosts](#win_toast-sends-toast-windows-notification-to-logged-in-users-on-windows-10-or-later-hosts)
    - [`win_unzip`: Unzips compressed files and archives on the Windows node](#win_unzip-unzips-compressed-files-and-archives-on-the-windows-node)
    - [`win_updates`: Download and install Windows updates](#win_updates-download-and-install-windows-updates)
    - [`win_uri`: Interacts with webservices](#win_uri-interacts-with-webservices)
    - [`win_user`: Manages local Windows user accounts](#win_user-manages-local-windows-user-accounts)
    - [`win_user_profile`: Manages the Windows user profiles.](#win_user_profile-manages-the-windows-user-profiles)
    - [`win_user_right`: Manage Windows User Rights](#win_user_right-manage-windows-user-rights)
    - [`win_wait_for`: Waits for a condition before continuing](#win_wait_for-waits-for-a-condition-before-continuing)
    - [`win_wait_for_process`: Waits for a process to exist or not exist before continuing.](#win_wait_for_process-waits-for-a-process-to-exist-or-not-exist-before-continuing)
    - [`win_wakeonlan`: Send a magic Wake-on-LAN (WoL) broadcast packet](#win_wakeonlan-send-a-magic-wake-on-lan-wol-broadcast-packet)
    - [`win_webpicmd`: Installs packages using Web Platform Installer command-line](#win_webpicmd-installs-packages-using-web-platform-installer-command-line)
    - [`win_whoami`: Get information about the current user and process](#win_whoami-get-information-about-the-current-user-and-process)
    - [`win_xml`: Manages XML file content on Windows hosts](#win_xml-manages-xml-file-content-on-windows-hosts)
    - [`win_zip`: Compress file or directory as zip archive on the Windows node](#win_zip-compress-file-or-directory-as-zip-archive-on-the-windows-node)
- [Dependencies](#dependencies)
- [Example Playbook](#example-playbook)

## Requirements

<!-- Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required. -->

## Role Variables

<!--  A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well. -->

### `win_roles`

Use the list `win_roles` to configure Ansible roles form this collection that
the provisioning of this role will be dependent on for example:

```yaml
win_roles`:
  - secrets
```

The roles you can include are:

* `secrets`

see [meta/main.yml](./meta/main.yml)

### `win_<module>` and `win_resources_types`

To harness the power of the 117+ modules supported by this role, employ the
`win_<module>` variable alongside the crucial `win_resource_types` variable.
Below is a practical example that details setting up a service account named
`svc_gis_platform`:

```yaml
win_user:
  - name: svc_gis_platform
    fullname: GIS Platform Service Account
    description: FME, ArcGIS
    password: Supersecret!
    password_expired: no

win_group_membership:
  - name: Administrators
    members:
      - .\svc_gis_platform
    notify: Restart service

win_user_right:
  - name: SeServiceLogonRight
    users: svc_gis_platform
```

The `win_resource_types` variable is essential for listing the modules to
provision. To enable the service account configuration shown above, you must
specify:

```yaml
win_resources_types:
  - win_user
  - win_group_membership
  - win_user_right
```

This variable determines the modules to use and the order of their execution:
starting with account creation, updating group memberships, and finally altering
user rights.

For complex setups, win_resource_types can be formatted as a dictionary,
enabling diverse module configurations across different hosts or groups within a
single playbook. An example with an extra key named `whatever` (a placeholder for
any string) is shown below:

```yaml
win_resources_types:
  whatever:
    - win_user
    - win_group_membership
    - win_user_right
```

For further information on utilizing dictionaries for more advanced use cases,
please consult the section [Using Dictionaries and
Lists](#using-dictionaries-and-lists) explains the use of dictionaries.

### `win_resources`

The `win_resources` variable offers greater control and flexibility compared to
`win_<module>` by allowing precise management over task execution order and module
reuse at various stages, thus facilitating dependency management.

Creating the `svc_gis_platform` service account using win_resources is
illustrated as follows:

```yaml
win_resources:
  - name: svc_gis_platform
    type: win_user
    fullname: GIS Platform Service Account
    description: FME, ArcGIS
    password: Supersecret!
    password_expired: no
  - name: Administrators
    type: win_group_membership
    members:
      - .\svc_gis_platform
    notify: Restart service
  - name: SeServiceLogonRight
    type: win_user_right
    users: svc_gis_platform
```

Here, the `type` key specifies the module for resource provisioning. Similar to
`win_resource_types`, the `win_resources` variable can also utilize a dictionary
structure for more sophisticated scenarios. Further details on advanced
dictionary use are available in the [Using Dictionaries and
Lists](#using-dictionaries-and-lists) section.

### Understanding `type` vs `module` in `win_resources`

In the `win_resources` context, the term `module` serves as an alias for `type`.
This aliasing is particularly useful when working with Ansible modules that
already include a `type` parameter, which you might need to use separately. An
example of such a module is [`win_xml`](#win_xml).

Consider the following configuration for the tomcat_win_resources:

```yaml
tomcat_win_resources:
  tomcat:
    - name: Configure context.xml
      module: win_xml
      type: element
      path: "{{ tomcat_home }}/conf/context.xml"
      xpath: /Context
      fragment: >-
        <Valve className="org.apache.catalina.authenticator.SSLAuthenticator"
        disableProxyCaching="true" />
```

In this example:

1. `module: win_xml` specifies that the `win_xml` Ansible module is used.
2. `type: element` indicates the nature of the operation within the `win_xml`
   module's context, in this case, working with an XML element.

By having an alias `module` for `type`, `win_resources` allows for more
versatile configurations, especially in cases where the chosen module requires a
`type` parameter for its own purposes.

### `win_proxy_password`, `win_proxy_url` and `win_proxy_username`

You can use the `win_proxy_password`, `win_proxy_url`, and `win_proxy_username`
variables to set up global proxy server configuration for Ansible modules. These
variables allow you to streamline proxy settings across various modules, such as
[`win_chocolatey`](#win_chocolatey) uses these variables.

For instance, instead of specifying proxy settings individually like this:

```yaml
win_chocolatey:
    - name: NuGet and Firefox ( development)
      type: win_chocolatey
      resources:
        - name: nuget.commandline
          proxy_url: http://proxy.example.com:3030
        - name: firefox
          proxy_url: http://proxy.example.com:3030
```
You can simplify your configuration by using the global proxy variables like so:

```yaml
win_proxy_url: http://proxy.example.com:3030
win_chocolatey:
    - name: NuGet and Firefox ( development)
      type: win_chocolatey
      resources:
        - name: nuget.commandline
        - name: firefox
```

By configuring the global proxy settings, you ensure consistent and efficient
proxy usage across multiple Ansible modules.

### Leveraging `resources` and `defaults` for Efficiency

The `win_resources` and `win_<module>` variables support the `resources` and
`defaults` keys to streamline your code, adhering to the **DRY** (Don't Repeat
Yourself) and **DIE** (Duplication is Evil) principles in software development.
These principles advocate for reducing repetition to enhance maintainability and
efficiency. The following example demonstrates their application:

```yaml
win_resources:
  - name: Create shares
    type: win_share
    defaults:
      write: true
      full: "{{ gs_software_repository_ansible['username'] }},vagrant"
    resources:
      - name: "{{ gs_software_repository_ansible['name'] }}"
        path: "{{ gs_software_repository_ansible['path'] }}"
        description: Ansible Software Repository Share
      - name: "{{ gs_cacerts2_share['name'] }}"
        path: "{{ gs_cacerts2_share['path'] }}"
        description: Ansible Certificates Share
```

Without utilizing `resources` and `defaults`, the configuration would require
duplication for each resource, as seen below:

```yaml
win_resources:
  - name: "{{ gs_software_repository_ansible['name'] }}"
    type: win_share
    write: true
    full: "{{ gs_software_repository_ansible['username'] }},vagrant"
    path: "{{ gs_software_repository_ansible['path'] }}"
    description: Ansible Software Repository Share
  - name: "{{ gs_cacerts2_share['name'] }}"
    type: win_share
    write: true
    full: "{{ gs_software_repository_ansible['username'] }},vagrant"
    path: "{{ gs_cacerts2_share['path'] }}"
    description: Ansible Certificates Share
```

### Using Dictionaries and Lists

In this section, we discuss the flexibility of using dictionaries or lists to
define variables in this Ansible role. The choice between
the two depends on your needs, and utilizing a dictionary can offer a
significant advantage when dealing with the configuration of different server
groups. See the guideline
[Managing Dictionary Merging](http://c2platform.org/docs/guidelines/coding/merge/)
for more information about this advanced Ansible feature.

Consider a scenario where you have defined two server groups: `windows` and
`age_pro`. You want to configure the `win_chocolatey` variable to perform the
following tasks:

1. Install NuGet on all Microsoft Windows servers (i.e., all servers in the
   `windows` group).
1. Install Firefox on all ArcGIS Pro servers (i.e., all servers in the `age_pro`
   group).

#### Using Lists (Less Flexible)

If you choose to use a simple list, you would add the following to the
`group_vars/windows/main.yml` file:

```yaml
  win_chocolatey:
    - name: nuget.commandline
```

Similarly, in the `group_vars/age_pro/main.yml` file, you would add:

```yaml
  win_chocolatey:
    - name: firefox
```

However, it's important to note that if a server belongs to both the `windows`
and `age_pro` groups, only one package will be installed, either Firefox or the
NuGet package, but not both. Lists cannot be merged together.

#### Using Dictionaries (More Flexible)

To achieve better flexibility and the ability to merge configurations for
servers in multiple groups, you can use dictionaries. In the
`group_vars/windows/main.yml` file, you would define `win_chocolatey` like this:

```yaml
win_chocolatey:
  windows:
    - name: nuget.commandline
```

In the `group_vars/age_pro/main.yml` file, you would define win_chocolatey as
follows:

```yaml
  win_chocolatey:
    age_pro:
      - name: firefox
```

When servers are part of both groups, Ansible will intelligently merge the
`win_chocolatey` variable to include both configurations, resulting in the
following combined configuration:

```yaml
  win_chocolatey:
    windows:
      - name: nuget.commandline
    age_pro:
      - name: firefox
```

This approach ensures that servers in multiple groups receive the intended
packages without conflicts or overwrites, providing a more robust and flexible
configuration management solution.

**Note:** You have the flexibility to employ any key names of your choice to categorize the lists. In the example below, we've utilized `whatever` and `anykey` as demonstration:

```yaml
  win_chocolatey:
    whatever:
      - name: nuget.commandline
    anykey:
      - name: firefox
```
### Modules

<!-- start supported_modules -->
This roles supports a selection of 120 modules
from the collections `ansible.windows`, `community.windows`, `chocolatey.chocolatey`, `c2platform.wincore`, `ansible.builtin`. This section
contains simple examples on how these modules can be used via the `win_resources` and
`win_<module_name>` variables.


#### `fail`: Fail with custom message

```yaml
win_fail:
  - msg: The system may not be provisioned according to the CMDB status

```

```yaml
win_resources:
  - module: fail
    msg: The machine failed to provision according to the CMDB status

```

For more examples and information on options see
[`ansible.builtin.fail`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/fail_module.html).
#### `include_role`: Load and execute a role

```yaml
win_include_role:
    - name: myrole
    - public: true

```

```yaml
win_resources:
    - module: include_role
      name: myrole
      public: true

```

For more examples and information on options see
[`ansible.builtin.include_role`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/include_role_module.html).
#### `psexec`: Runs commands on a remote Windows host based on the PsExec model

```yaml
win_psexec:
  - hostname: server
    connection_username: username
    connection_password: password
    executable: cmd.exe
    arguments: /c echo Hello World

```

```yaml
win_resources:
  - module: psexec
    hostname: server
    connection_username: username
    connection_password: password
    executable: cmd.exe
    arguments: /c echo Hello World

```

For more examples and information on options see
[`community.windows.psexec`](https://docs.ansible.com/ansible/latest/collections/community/windows/psexec_module.html).
#### `slurp`: Slurps a file from remote nodes

```yaml
win_slurp:
  whatever:
    - option1: value1
      option2: value2
    - option3: value3
      option4: value4

```

```yaml
win_resources:
  whatever:
    - module: slurp
      src: path/to/file.txt

```

For more examples and information on options see
[`ansible.windows.slurp`](https://docs.ansible.com/ansible/latest/collections/ansible/windows/slurp_module.html).
#### `win_acl`: Set file/directory/registry permissions for a system user or group

```yaml
win_acl:
  whatever:
    - user: Fed-Phil
      path: C:\\Important\\Executable.exe
      type: deny
      rights: ExecuteFile,Write

```

```yaml
win_resources:
  whatever:
    - module: win_acl
      inherit: ContainerInherit, ObjectInherit
      path: C:\\inetpub\\wwwroot\\MySite
      propagation: 'None'
      rights: FullControl
      state: present
      type: allow
      user: IIS_IUSRS

```

For more examples and information on options see
[`ansible.windows.win_acl`](https://docs.ansible.com/ansible/latest/collections/ansible/windows/win_acl_module.html).
#### `win_acl_inheritance`: Change ACL inheritance

```yaml
win_acl_inheritance:
  whatever:  # optional extra level for dictionary merging
    - path: C:\apache
      state: absent

```

```yaml
win_resources:
  whatever:  # optional extra level for dictionary merging
    - module: win_acl_inheritance
      path: C:\apache
      reorganize: true

```

For more examples and information on options see
[`ansible.windows.win_acl_inheritance`](https://docs.ansible.com/ansible/latest/collections/ansible/windows/win_acl_inheritance_module.html).
#### `win_audit_policy_system`: Used to make changes to the system wide Audit Policy

```yaml
win_audit_policy_system:
  - category: Account logon events
    audit_type: success, failure

```

```yaml
win_resources:
  - module: win_audit_policy_system
    audit_type: success
    category: Account logon events

```

For more examples and information on options see
[`community.windows.win_audit_policy_system`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_audit_policy_system_module.html).
#### `win_audit_rule`: Adds an audit rule to files, folders, or registry keys

```yaml
win_audit_rule:
  - path: "C:\\inetpub\\wwwroot\\website"
    audit_flags: ["Success", "Failure"]
    inheritance_flags: ["ContainerInherit", "ObjectInherit"]
    rights: ["write", "delete", "changepermissions"]
    state: "present"
    user: "BUILTIN\\Users"
  - path: "C:\\inetpub\\wwwroot\\website\\web.config"
    audit_flags: ["Success", "Failure"]
    inheritance_flags: ["None"]
    rights: ["write", "delete", "changepermissions"]
    state: "present"
    user: "BUILTIN\\Users"

```

For more examples and information on options see
[`community.windows.win_audit_rule`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_audit_rule_module.html).
#### `win_auto_logon`: Adds or Sets auto logon registry keys.

```yaml
win_auto_logon:
  - username: User1
    password: str0ngp@ssword

```

```yaml
win_resources:
  - module: win_auto_logon
    logon_count: 5
    state: present
    username: User1

```

For more examples and information on options see
[`community.windows.win_auto_logon`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_auto_logon_module.html).
#### `win_certificate`: Manages certificates in Windows

```yaml
win_certificate:
  - dns_name: "{{ ansible_hostname }}"
    validity_days: 3650
    state: present
    # tmp_file_path: D:\\Temp\\ExportedCertificate.cer
  register: _cert

```

```yaml
win_resources:
  - module: win_certificate
    dns_name: "www.example.com"
    validity_days: 365
    state: present

```

For more examples and information on options see
[`c2platform.wincore.win_certificate`](https://docs.ansible.com/ansible/latest/collections/c2platform/wincore/win_certificate_module.html).
#### `win_certificate_info`: Get information on certificates from a Windows Certificate Store

```yaml
win_certificate_info:
  whatever:
    - thumbprint: BD7AF104CF1872BDB518D95C9534EA941665FD27
      store_location: CurrentUser

```

```yaml
win_resources:
  whatever:
    - module: win_certificate_info
      thumbprint: BD7AF104CF1872BDB518D95C9534EA941665FD27
      store_location: LocalMachine

```

For more examples and information on options see
[`community.windows.win_certificate_info`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_certificate_info_module.html).
#### `win_certificate_store`: Manages the certificate store
The `win_certificate_store` dictionary is utilized for managing certificates
within the Windows certificate store, leveraging the
[`ansible.windows.win_certificate_store`](https://docs.ansible.com/ansible/latest/collections/ansible/windows/win_certificate_store_module.html))
module. This module facilitates the import, export, and removal of certificates
and keys from a local certificate store on Windows systems. It's important to
note that this module does not generate new certificates but rather manages
existing ones across a variety of formats including PEM, DER, P7B, and PKCS12
(PFX), with capabilities to export in PEM, DER, and PKCS12 formats.

**Key Parameters:**

- **`path`**: Defines the path to the certificate file for import or export
  operations.
- **`state`**: Sets the desired state of the certificate, with options including
  `present`, `absent`, or `exported`.
- **`store_name`**: Specifies the store for importing or exporting the
  certificate, such as `My` for personal certificates or `Root` for root CAs.
- **`store_location`**: Determines the certificate store location, either
  `CurrentUser` or `LocalMachine`.
- **`password`**: Provides the password for protected certificates, particularly
  relevant for PKCS12 format.

This module is pivotal for automating the management of certificates on Windows,
ensuring secure communication and trusted relationships between systems.

For more examples and information on options see
[`ansible.windows.win_certificate_store`](https://docs.ansible.com/ansible/latest/collections/ansible/windows/win_certificate_store_module.html).
#### `win_chocolatey`: Manage packages using chocolatey
The `win_chocolatey` module is designed to simplify the management of Chocolatey
packages within the Ansible ecosystem. You can find this module in the
[chocolatey.chocolatey](https://galaxy.ansible.com/chocolatey/chocolatey)
collection.

Here's an example of how to use the `win_chocolatey` list to install specific
Chocolatey packages:

```yaml
  win_chocolatey:
    - name: notepadplusplus
      version: '6.6'
    - name: 7zip
      version: '22.1'
```

These list items support all the arguments available in the `win_chocolatey`
module, including for example the `proxy_url` parameter. However, you can further enhance the
module's functionality by utilizing the
[`win_proxy_password`, `win_proxy_url` and `win_proxy_username`](#win_proxy_password-win_proxy_url-and-win_proxy_username)
variables to manage proxy settings globally.

For more examples and information on options see
[`chocolatey.chocolatey.win_chocolatey`](https://docs.ansible.com/ansible/latest/collections/chocolatey/chocolatey/win_chocolatey_module.html).
#### `win_chocolatey_config`: Manages Chocolatey config settings

```yaml
win_chocolatey_config:
  - name: cacheLocation
    state: present
    value: D:\chocolatey_temp

```

```yaml
win_resources:
  - module: win_chocolatey_config
    name: cacheLocation
    state: present
    value: D:\chocolatey_temp

```

For more examples and information on options see
[`chocolatey.chocolatey.win_chocolatey_config`](https://docs.ansible.com/ansible/latest/collections/chocolatey/chocolatey/win_chocolatey_config_module.html).
#### `win_chocolatey_feature`: Manages Chocolatey features

```yaml
win_chocolatey_feature:
  whatever:
    - name: checksumFiles
      state: disabled
    - name: stopOnFirstPackageFailure
      state: enabled

```

```yaml
win_resources:
  whatever:
    - module: win_chocolatey_feature
      name: checksumFiles
      state: disabled

```

For more examples and information on options see
[`chocolatey.chocolatey.win_chocolatey_feature`](https://docs.ansible.com/ansible/latest/collections/chocolatey/chocolatey/win_chocolatey_feature_module.html).
#### `win_chocolatey_source`: Manages Chocolatey sources

```yaml
win_chocolatey_source:
  whatever:
    - name: internal repo
      state: present
      source: http://chocolatey-server/chocolatey

```

```yaml
win_resources:
  whatever:
    - module: win_chocolatey_source
      name: internal repo
      state: present
      source: https://chocolatey-server/chocolatey

```

For more examples and information on options see
[`chocolatey.chocolatey.win_chocolatey_source`](https://docs.ansible.com/ansible/latest/collections/chocolatey/chocolatey/win_chocolatey_source_module.html).
#### `win_command`: Executes a command on a remote Windows node

```yaml
win_command:
  whatever:
    - cmd: whoami.exe /all

```

```yaml
win_resources:
  whatever:
    - module: win_command
      cmd: whoami.exe /all

```

For more examples and information on options see
[`ansible.windows.win_command`](https://docs.ansible.com/ansible/latest/collections/ansible/windows/win_command_module.html).
#### `win_computer_description`: Set windows description, owner and organization

```yaml
win_computer_description:
  whatever:  # optional extra level for dictionary merging
    - description: Best Box
      owner: RusoSova
      organization: MyOrg

```

```yaml
win_resources:
  whatever:  # optional extra level for dictionary merging
    - module: win_computer_description
      description: This is my Windows machine

```

For more examples and information on options see
[`community.windows.win_computer_description`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_computer_description_module.html).
#### `win_copy`: Copies files to remote locations on windows hosts

```yaml
win_copy:
  - src: /srv/myfiles/foo.conf
    dest: C:\\Temp\\renamed-foo.conf

```

```yaml
win_resources:
  - module: win_copy
    src: /srv/myfiles/foo.conf
    dest: C:\\Temp\\renamed-foo.conf

```

For more examples and information on options see
[`ansible.windows.win_copy`](https://docs.ansible.com/ansible/latest/collections/ansible/windows/win_copy_module.html).
#### `win_credential`: Manages Windows Credentials in the Credential Manager

```yaml
win_credential:
  example_credential_1:
    - name: server.domain.com
      type: domain_password
      username: DOMAIN\username
      secret: Password01
      state: present


```

```yaml
win_resources:
  example_resources:
    - module: win_credential
      name: smbhost
      type: generic_password
      username: smbuser
      secret: smbuser
      state: present

```

For more examples and information on options see
[`community.windows.win_credential`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_credential_module.html).
#### `win_data_deduplication`: Module to enable Data Deduplication on a volume.

```yaml
win_data_deduplication:
  whatever:
    - drive_letter: 'D'
      state: present

```

```yaml
win_resources:
  whatever:
    - module: win_data_deduplication
      drive_letter: 'E'
      settings:
        no_compress: true
        minimum_file_age_days: 1
        minimum_file_size: 0

```

For more examples and information on options see
[`community.windows.win_data_deduplication`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_data_deduplication_module.html).
#### `win_defrag`: Consolidate fragmented files on local volumes

```yaml
win_defrag:
  whatever:
    - exclude_volumes: [ C, D ]
      freespace_consolidation: true

```

```yaml
win_resources:
  whatever:
    - module: win_defrag
      exclude_volumes: [ E, F ]
      include_volumes: [ Z ]
      priority: normal

```

For more examples and information on options see
[`community.windows.win_defrag`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_defrag_module.html).
#### `win_dhcp_lease`: Manage Windows Server DHCP Leases

```yaml
win_dhcp_lease:
  - type: reservation
    ip: 192.168.100.205
    scope_id: 192.168.100.0
    mac: 00:B1:8A:D1:5A:1F
    dns_hostname: "{{ ansible_inventory }}"
    description: Testing Server

```

```yaml
win_resources:
  - module: win_dhcp_lease
    ip: 192.168.100.205
    mac: 00:B1:8A:D1:5A:1F

```

For more examples and information on options see
[`community.windows.win_dhcp_lease`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_dhcp_lease_module.html).
#### `win_disk_facts`: Show the attached disks and disk information of the target host

```yaml
win_disk_facts:
  - filter: ['physical_disk', 'virtual_disk', 'partitions', 'volumes']

```

```yaml
win_resources:
  - module: win_disk_facts
    filter: ['physical_disk', 'virtual_disk', 'partitions', 'volumes']

```

For more examples and information on options see
[`community.windows.win_disk_facts`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_disk_facts_module.html).
#### `win_disk_image`: Manage ISO/VHD/VHDX mounts on Windows hosts

```yaml
win_disk_image:
  whatever:  # optional extra level for dictionary merging
    - image_path: C:\\install.iso
      state: present

```

```yaml
win_resources:
  whatever:  # optional extra level for dictionary merging
    - module: win_disk_image
      image_path: C:\\install.iso
      state: absent

```

For more examples and information on options see
[`community.windows.win_disk_image`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_disk_image_module.html).
#### `win_dns_client`: Configures DNS lookup on Windows hosts

```yaml
win_dns_client:
  - adapter_names: Ethernet
    dns_servers:
      - 192.168.34.5

```

```yaml
win_resources:
  - module: win_dns_client
    adapter_names: Ethernet
    dns_servers:
      - 192.168.34.5

```

For more examples and information on options see
[`ansible.windows.win_dns_client`](https://docs.ansible.com/ansible/latest/collections/ansible/windows/win_dns_client_module.html).
#### `win_dns_record`: Manage Windows Server DNS records

```yaml
win_dns_record:
  whatever:  # optional extra level for dictionary merging
    - name: "cgyl1404p.amer.example.com"
      type: "A"
      value: "10.1.1.1"
      zone: "amer.example.com"

```

```yaml
win_resources:
  whatever:  # optional extra level for dictionary merging
    - module: win_dns_record
      name: "cgyl1404p.amer.example.com"
      type: "A"
      value: "10.1.1.1"
      zone: "amer.example.com"

```

For more examples and information on options see
[`community.windows.win_dns_record`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_dns_record_module.html).
#### `win_dns_zone`: Manage Windows Server DNS Zones

```yaml
win_dns_zone:
  - name: wpinner.euc.vmware.com
    replication: domain
    type: primary
    state: present

```

```yaml
win_resources:
  - module: win_dns_zone
    dns_servers:
      - 10.245.51.100
      - 10.245.51.101
    name: jamals.euc.vmware.com
    type: forwarder

```

For more examples and information on options see
[`community.windows.win_dns_zone`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_dns_zone_module.html).
#### `win_dotnet_ngen`: Runs ngen to recompile DLLs after .NET  updates

```yaml
win_dotnet_ngen:
  whatever:  # optional extra level for dictionary merging
    - <option>: <module parameter>
      <option>: <module parameter>
    - <option>: <module parameter>
      <option>: <module parameter>

```

```yaml
win_resources:
  whatever:  # optional extra level for dictionary merging
    - module: win_dotnet_ngen
      <option>: <module option>
      <option>: <module option>

```

For more examples and information on options see
[`community.windows.win_dotnet_ngen`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_dotnet_ngen_module.html).
#### `win_dsc`: Invokes a PowerShell DSC configuration

```yaml
win_dsc:
  whatever:
    - resource_name: Archive
      Ensure: Present
      Path: C:\Temp\zipfile.zip
      Destination: C:\Temp\Temp2

```

```yaml
win_resources:
  whatever:
    - module: win_dsc
      free_form: |
        $port = (Get-Item -LiteralPath WSMan:\localhost\Client\DefaultPorts\HTTP).Value
        $onlinePorts = @(Get-ChildItem -LiteralPath WSMan:\localhost\Listener |
            Where-Object { 'Transport=HTTP' -in $_.Keys } |
            Get-ChildItem |
            Where-Object Name -eq Port |
            Select-Object -ExpandProperty Value)

        if ($port -notin $onlinePorts) {
            "The default client port $port is not set up as a WSMan HTTP listener, win_dsc will not work."
        }

```

For more examples and information on options see
[`ansible.windows.win_dsc`](https://docs.ansible.com/ansible/latest/collections/ansible/windows/win_dsc_module.html).
#### `win_environment`: Modify environment variables on windows hosts

```yaml
win_environment:
  TestVariable:
    - name: TestVariable
      value: Test value
      level: machine
    - name: TestVariable
      state: absent
      level: user
  CUSTOM_APP_VAR:
    - name: CUSTOM_APP_VAR
      value: Very important value
      level: machine
  ANOTHER_VAR:
    - name: ANOTHER_VAR
      value: '{{ my_ansible_var }}'
      level: machine

```

```yaml
win_resources:
  win_environment:
    - module: win_environment
      name: TestVariable
      value: Test value
      level: machine
    - module: win_environment
      name: TestVariable
      state: absent
      level: user
    - module: win_environment
      name: CUSTOM_APP_VAR
      value: Very important value
      level: machine
    - module: win_environment
      name: ANOTHER_VAR
      value: '{{ my_ansible_var }}'
      level: machine

```

For more examples and information on options see
[`ansible.windows.win_environment`](https://docs.ansible.com/ansible/latest/collections/ansible/windows/win_environment_module.html).
#### `win_eventlog`: Manage Windows event logs

```yaml
win_eventlog:
  whatever:
    - name: MyNewLog
      sources:
        - NewLogSource1
        - NewLogSource2
      state: present

```

```yaml
win_resources:
  whatever:
    - module: win_eventlog
      name: MySecondLog
      maximum_size: 16MB
      overflow_action: DoNotOverwrite
      state: present

```

For more examples and information on options see
[`community.windows.win_eventlog`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_eventlog_module.html).
#### `win_eventlog_entry`: Write entries to Windows event logs

```yaml
win_eventlog_entry:
  whatever:  # optional extra level for dictionary merging
    - log: MyNewLog
      source: NewLogSource1
      event_id: 1234
      message: This is a test log entry

```

```yaml
win_resources:
  whatever:  # optional extra level for dictionary merging
    - module: win_eventlog_entry
      category: 5
      entry_type: Error
      event_id: 5000
      log: AnotherLog
      message: An error has occurred
      source: MyAppSource
      raw_data: 10,20

```

For more examples and information on options see
[`community.windows.win_eventlog_entry`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_eventlog_entry_module.html).
#### `win_feature`: Installs and uninstalls Windows Features on Windows Server

```yaml
win_feature:
  whatever:  # optional extra level for dictionary merging
    - name: Telnet-Client
      state: present
    - name: Web-Common-HTTP
      include_sub_features: true

```

```yaml
win_resources:
  whatever:  # optional extra level for dictionary merging
    - module: win_feature
      name: Web-Server
      state: present

```

For more examples and information on options see
[`ansible.windows.win_feature`](https://docs.ansible.com/ansible/latest/collections/ansible/windows/win_feature_module.html).
#### `win_feature_info`: Gather information about Windows features

```yaml
win_feature_info:
  - name: DNS

```

```yaml
win_resources:
  - module: win_feature_info
    name: DNS

```

For more examples and information on options see
[`community.windows.win_feature_info`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_feature_info_module.html).
#### `win_file`: Creates, touches or removes files or directories

```yaml
win_file:
  whatever:  # optional extra level for dictionary merging
    - path: C:\Temp\foo.conf
      state: touch
    - path: C:\Temp\bar.conf
      state: file

```

```yaml
win_resources:
  whatever:  # optional extra level for dictionary merging
    - module: win_file
      path: C:\Temp\example_dir
      state: directory

```

For more examples and information on options see
[`ansible.windows.win_file`](https://docs.ansible.com/ansible/latest/collections/ansible/windows/win_file_module.html).
#### `win_file_compression`: Alters the compression of files and directories on NTFS partitions.

```yaml
win_file_compression:
  - path: C:\Logs
    state: present

```

```yaml
win_resources:
  - module: win_file_compression
    path: C:\business\reports
    state: present
    recurse: yes

```

For more examples and information on options see
[`community.windows.win_file_compression`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_file_compression_module.html).
#### `win_file_version`: Get DLL or EXE file build version

```yaml
win_file_version:
  whatever:
    - path: C:\\Windows\\System32\\cmd.exe

```

```yaml
win_resources:
  whatever:
    - module: win_file_version
      path: C:\\Windows\\System32\\cmd.exe

```

For more examples and information on options see
[`community.windows.win_file_version`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_file_version_module.html).
#### `win_firewall`: Enable or disable the Windows Firewall
See for example
[`group_vars/windows/main.yml`](https://gitlab.com/c2platform/rws/ansible-gis/-/blob/master/group_vars/windows/main.yml)
in [ansible-gis](https://gitlab.com/c2platform/rws/ansible-gis/) project.

For more examples and information on options see
[`community.windows.win_firewall`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_firewall_module.html).
#### `win_firewall_rule`: Windows firewall automation
See for example
[`group_vars/ag_server/firewall.yml`](https://gitlab.com/c2platform/rws/ansible-gis/-/blob/master/group_vars/ag_server/firewall.yml) in
[ansible-gis](https://gitlab.com/c2platform/rws/ansible-gis/) project.

For more examples and information on options see
[`community.windows.win_firewall_rule`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_firewall_rule_module.html).
#### `win_format`: Formats an existing volume or a new volume on an existing partition on Windows

```yaml
win_format:
  whatever:
    - drive_letter: D
      file_system: NTFS
      new_label: Formatted
      full: True

```

```yaml
win_resources:
  whatever:
    - module: win_format
      drive_letter: C
      allocation_unit_size: 4096
      compress: True

```

For more examples and information on options see
[`community.windows.win_format`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_format_module.html).
#### `win_get_url`: Downloads file from HTTP, HTTPS, or FTP to node

```yaml
win_get_url:
  - dest: C:\\Users\\RandomUser\\earthrise.jpg
    url: http://www.example.com/earthrise.jpg

```

```yaml
win_resources:
  - module: win_get_url
    dest: C:\\Users\\RandomUser\\earthrise.jpg
    url: http://www.example.com/earthrise.jpg

```

For more examples and information on options see
[`ansible.windows.win_get_url`](https://docs.ansible.com/ansible/latest/collections/ansible/windows/win_get_url_module.html).
#### `win_group`: Add and remove local groups

```yaml
win_group:
  my_group:
    - name: developers
      description: Developers group
      state: present

```

```yaml
win_resources:
  my_group:
    - module: win_group
      name: operators
      state: present

```

For more examples and information on options see
[`ansible.windows.win_group`](https://docs.ansible.com/ansible/latest/collections/ansible/windows/win_group_module.html).
#### `win_group_membership`: Manage Windows local group membership
```yaml
win_group_membership:
  - name: Administrators
    members:
      - .\gis-backup-operator
    notify: Restart service
```

For more examples and information on options see
[`ansible.windows.win_group_membership`](https://docs.ansible.com/ansible/latest/collections/ansible/windows/win_group_membership_module.html).
#### `win_hostname`: Manages local Windows computer name

```yaml
win_hostname:
  whatever:  # optional extra level for dictionary merging
    - name: sample-hostname
win_resources:
  whatever:  # optional extra level for dictionary merging
    - module: win_hostname
      name: "{{ item['name'] }}"

```

For more examples and information on options see
[`ansible.windows.win_hostname`](https://docs.ansible.com/ansible/latest/collections/ansible/windows/win_hostname_module.html).
#### `win_hosts`: Manages hosts file entries on Windows.
```yaml
win_hosts:
  - canonical_name: gsd-agserver1.internal.c2platform.org
    ip_address: 1.1.8.100
    action: add
  - canonical_name: gsd-agportal1.internal.c2platform.org
    ip_address: 1.1.8.103
    action: add
```

For more examples and information on options see
[`community.windows.win_hosts`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_hosts_module.html).
#### `win_hotfix`: Install and uninstalls Windows hotfixes

```yaml
win_hotfix:
  - hotfix_kb: KB123456
    source: C:\temp\hotfix.msu
    state: present

```

```yaml
win_resources:
  - module: win_hotfix
    hotfix_kb: KB123456
    source: C:\temp\hotfix.msu
    state: absent

```

For more examples and information on options see
[`community.windows.win_hotfix`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_hotfix_module.html).
#### `win_http_proxy`: Manages proxy settings for WinHTTP

```yaml
win_http_proxy:
  - proxy: hostname

```

```yaml
win_resources:
  - module: win_http_proxy
    proxy: hostname

```

For more examples and information on options see
[`community.windows.win_http_proxy`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_http_proxy_module.html).
#### `win_iis_virtualdirectory`: Configures a virtual directory in IIS

```yaml
win_iis_virtualdirectory:
  whatever:  # optional extra level for dictionary merging
    - name: somedirectory
      site: somesite
      state: present
      physical_path: C:\\virtualdirectory\\some

```

```yaml
win_resources:
  whatever:  # optional extra level for dictionary merging
    - module: win_iis_virtualdirectory
      name: somedirectory
      site: somesite
      state: absent

```

For more examples and information on options see
[`community.windows.win_iis_virtualdirectory`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_iis_virtualdirectory_module.html).
#### `win_iis_webapplication`: Configures IIS web applications

```yaml
win_iis_webapplication:
  my_webapp:
    - name: myapp
      site: mysite
      state: present
      physical_path: C:\inetpub\wwwroot\myapp

```

```yaml
win_resources:
  my_resources:
    - module: win_iis_webapplication
      name: anotherapp
      site: anothersite
      state: absent
      physical_path: C:\inetpub\wwwroot\anotherapp

```

For more examples and information on options see
[`community.windows.win_iis_webapplication`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_iis_webapplication_module.html).
#### `win_iis_webapppool`: Configure IIS Web Application Pools

```yaml
win_iis_webapppool:
  whatever:  # optional extra level for dictionary merging
    - name: DefaultAppPool
      state: present
    - name: AppPool
      state: started
      attributes:
        managedRuntimeVersion: v4.0
        autoStart: no

```

```yaml
win_resources:
  whatever:  # optional extra level for dictionary merging
    - module: win_iis_webapppool
      name: AnotherAppPool
      state: started
      attributes:
        managedRuntimeVersion: v4.0
        autoStart: no

```

For more examples and information on options see
[`community.windows.win_iis_webapppool`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_iis_webapppool_module.html).
#### `win_iis_webbinding`: Configuring IIS Web Bindings

Basic HTTPS Binding Example:

```yaml
win_iis_webbinding:
  - name: Default Web Site
    port: 9090
    state: present
```

Using Certificate Hash for HTTPS:

```yaml
win_resources:
  - module: win_iis_webbinding
    name: Default Web Site
    port: 443
    protocol: https
    ip: 127.0.0.1
    certificate_hash: B0D0FA8408FC67B230338FCA584D03792DA73F4C
    state: present
```

One limitation of the standard `win_iis_webbinding` module is its reliance on
`certificate_hash` for certificate selection, which might not be the most
user-friendly. To address this, this role introduces the ability to use
`certificate_friendly_name`, providing a more intuitive way to manage
certificates by their logical names:

```yaml
# Define the friendly name for the certificate
gs_geoweb_certificate_friendly_name: geoweb

win_resources:
  # Create or ensure the presence of a certificate with a friendly name
  - name: Create Geoweb Certificate
    type: win_certificate
    friendly_name: "{{ gs_geoweb_certificate_friendly_name }}"
    dns_name: "{{ inventory_hostname }}.internal.c2platform.org"
    validity_days: 3650
    state: present
  # Configure IIS HTTPS binding using the friendly name of the certificate
  - module: win_iis_webbinding
    name: Default Web Site
    port: 443
    protocol: https
    ip: 127.0.0.1
    certificate_friendly_name: "{{ gs_geoweb_certificate_friendly_name }}"
    state: present
```

This approach simplifies managing SSL/TLS certificates for IIS by allowing
bindings to reference certificates by a name that is easy to remember and
manage, enhancing readability and maintenance of your Ansible playbooks.

For comprehensive details on additional options and more examples, refer to:
[`community.windows.win_iis_webbinding` Ansible Documentation](https://docs.ansible.com/ansible/latest/collections/community/windows/win_iis_webbinding_module.html)


#### `win_iis_website`: Configures a IIS Web site

```yaml
win_iis_website:
  - name: Acme IIS site
    state: started
    port: 80
    ip: 127.0.0.1
    hostname: acme.local
    application_pool: acme
    physical_path: C:\\sites\\acme
    parameters: "logfile.directory:C:\\sites\\logs"

```

```yaml
win_resources:
  - module: win_iis_website
    name: Default Web Site
    state: absent

```

For more examples and information on options see
[`community.windows.win_iis_website`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_iis_website_module.html).
#### `win_inet_proxy`: Manages proxy settings for WinINet and Internet Explorer

```yaml
win_inet_proxy:
  whatever:  # optional extra level for dictionary merging
    - auto_detect: yes
      auto_config_url: http://proxy.ansible.com/proxy.pac

```

```yaml
win_resources:
  whatever:  # optional extra level for dictionary merging
    - module: win_inet_proxy
      auto_detect: yes
      auto_config_url: http://proxy.ansible.com/proxy.pac

```

For more examples and information on options see
[`community.windows.win_inet_proxy`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_inet_proxy_module.html).
#### `win_initialize_disk`: Initializes disks on Windows Server

```yaml
win_initialize_disk:
  whatever:  # optional extra level for dictionary merging
    - disk_number: 1
      style: mbr

```

```yaml
win_resources:
  whatever:  # optional extra level for dictionary merging
    - module: win_initialize_disk
      disk_number: 2
      force: yes

```

For more examples and information on options see
[`community.windows.win_initialize_disk`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_initialize_disk_module.html).
#### `win_lineinfile`: Ensure a particular line is in a file, or replace an existing line using a back-referenced regular expression

```yaml
win_lineinfile:
  - path: C:\\Temp\\example.conf
    regex: '^name='
    line: 'name=JohnDoe'

```

```yaml
win_resources:
  - module: win_lineinfile
    path: C:\\Temp\\example.conf
    regex: '^name='
    line: 'name=JohnDoe'

```

For more examples and information on options see
[`community.windows.win_lineinfile`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_lineinfile_module.html).
#### `win_listen_ports_facts`: Recopilates the facts of the listening ports of the machine

```yaml
win_listen_ports_facts:
  - date_format: '%Y'
    tcp_filter:
      - Established

```

```yaml
win_resources:
  - module: win_listen_ports_facts
    date_format: '%c'
    tcp_filter:
      - Listen

```

For more examples and information on options see
[`community.windows.win_listen_ports_facts`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_listen_ports_facts_module.html).
#### `win_mapped_drive`: Map network drives for users

```yaml
win_mapped_drive:
  - letter: Z
    path: '\\\\domain\\appdata\\accounting'

```

```yaml
win_resources:
  - module: win_mapped_drive
    letter: Z
    path: '\\\\domain\\appdata\\accounting'

```

For more examples and information on options see
[`community.windows.win_mapped_drive`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_mapped_drive_module.html).
#### `win_msg`: Sends a message to logged in users on Windows hosts

```yaml
win_msg:
  - display_seconds: 60
    msg: Automated upgrade about to start. Please save your work and log off before {{ deployment_start_time }}

```

```yaml
win_resources:
  - module: win_msg
    display_seconds: 30
    msg: System will be restarted in 5 minutes for maintenance

```

For more examples and information on options see
[`community.windows.win_msg`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_msg_module.html).
#### `win_net_adapter_feature`: Enable or disable certain network adapters.

```yaml
win_net_adapter_feature:
  whatever:  # optional extra level for dictionary merging
    - interface:
      - 'Ethernet0'
      - 'Ethernet1'
      state: enabled
      component_id:
      - ms_tcpip6
      - ms_server

```

```yaml
win_resources:
  whatever:  # optional extra level for dictionary merging
    - module: win_net_adapter_feature
      component_id: ms_tcpip6
      interface: '*'
      state: enabled

```

For more examples and information on options see
[`community.windows.win_net_adapter_feature`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_net_adapter_feature_module.html).
#### `win_netbios`: Manage NetBIOS over TCP/IP settings on Windows.

```yaml
win_netbios:
  - state: disabled
    adapter_names:
      - Ethernet2

```

```yaml
win_resources:
  - module: win_netbios
    state: enabled
    adapter_names:
      - Public
      - Backup

```

For more examples and information on options see
[`community.windows.win_netbios`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_netbios_module.html).
#### `win_nssm`: Install a service using NSSM

```yaml
win_nssm:
  whatever:
    - name: foo_service
      application: C:\\windows\\foo.exe

```

```yaml
win_resources:
  whatever:
    - module: win_nssm
      name: bar_service
      application: C:\\windows\\bar.exe

```

For more examples and information on options see
[`community.windows.win_nssm`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_nssm_module.html).
#### `win_optional_feature`: Manage optional Windows features

```yaml
win_optional_feature:
  - name: NetFx3
    state: present

```

```yaml
win_resources:
  - module: win_optional_feature
    name: NetFx3
    state: present

```

For more examples and information on options see
[`ansible.windows.win_optional_feature`](https://docs.ansible.com/ansible/latest/collections/ansible/windows/win_optional_feature_module.html).
#### `win_owner`: Set owner

```yaml
win_owner:
  - path: C:\\apache
    user: apache
    recurse: true

```

```yaml
win_resources:
  - module: win_owner
    path: C:\\apache
    user: SYSTEM
    recurse: false

```

For more examples and information on options see
[`ansible.windows.win_owner`](https://docs.ansible.com/ansible/latest/collections/ansible/windows/win_owner_module.html).
#### `win_package`: Installs/uninstalls an installable package

```yaml
win_package:
  - path: http://download.microsoft.com/download/1/6/B/16B06F60-3B20-4FF2-B699-5E9B7962F9AE/VSU_4/vcredist_x64.exe
    product_id: '{CF2BEA3C-26EA-32F8-AA9B-331F7E34BA97}'
    arguments: /install /passive /norestart
  - path: http://download.microsoft.com/download/1/6/B/16B06F60-3B20-4FF2-B699-5E9B7962F9AE/VSU_4/vcredist_x64.exe
    product_id: '{CF2BEA3C-26EA-32F8-AA9B-331F7E34BA97}'
    arguments:
      - /install
      - /passive
      - /norestart

```

```yaml
win_resources:
  - module: win_package
    path: https://download.visualstudio.microsoft.com/download/pr/9665567e-f580-4acd-85f2-bc94a1db745f/vs_BuildTools.exe
    product_id: '{D1437F51-786A-4F57-A99C-F8E94FBA1BD8}'
    arguments:
      - --norestart
      - --passive
      - --wait
      - --add
      - Microsoft.Net.Component.4.6.1.TargetingPack
      - --add
      - Microsoft.Net.Component.4.6.TargetingPack

```

For more examples and information on options see
[`ansible.windows.win_package`](https://docs.ansible.com/ansible/latest/collections/ansible/windows/win_package_module.html).
#### `win_pagefile`: Query or change pagefile configuration

```yaml
win_pagefile:
  - drive: C
    initial_size: 1024
    maximum_size: 1024
    state: present

```

```yaml
win_resources:
  - module: win_pagefile
    drive: D
    initial_size: 2048
    maximum_size: 2048
    state: present

```

For more examples and information on options see
[`community.windows.win_pagefile`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_pagefile_module.html).
#### `win_partition`: Creates, changes and removes partitions on Windows Server

```yaml
win_partition:
  - drive_letter: D
    partition_size: 5 GiB
    disk_number: 1
  - drive_letter: E
    partition_size: -1
    partition_number: 1
    disk_number: 1
    state: present
    active: true
    mbr_type: fat32

```

```yaml
win_resources:
  - module: win_partition
    drive_letter: F
    partition_size: 10 GiB
    disk_number: 2
    state: absent

```

For more examples and information on options see
[`community.windows.win_partition`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_partition_module.html).
#### `win_path`: Manage Windows path environment variables

```yaml
win_path:
  - name: PATH
    elements:
      - '%SystemRoot%\\system32'
      - '%SystemRoot%\\system32\\WindowsPowerShell\\v1.0'

```

```yaml
win_resources:
  - module: win_path
    name: PATH
    elements:
      - '%SystemRoot%\\system32'
      - '%SystemRoot%\\system32\\WindowsPowerShell\\v1.0'

```

For more examples and information on options see
[`ansible.windows.win_path`](https://docs.ansible.com/ansible/latest/collections/ansible/windows/win_path_module.html).
#### `win_pester`: Run Pester tests on Windows hosts

```yaml
win_pester:
  - path: C:\\Pester\\test01.test.ps1
    version: 4.1.0

```

```yaml
win_resources:
  - module: win_pester
    path: C:\\Pester\\test04.test.ps1
    test_parameters:
      Process: lsass
      Service: bits

```

For more examples and information on options see
[`community.windows.win_pester`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_pester_module.html).
#### `win_ping`: A windows version of the classic ping module

```yaml
win_ping:
  whatever:  # optional extra level for dictionary merging
    - data: "pong"

```

```yaml
win_resources:
  whatever:  # optional extra level for dictionary merging
    - module: win_ping
      data: "pong"

```

For more examples and information on options see
[`ansible.windows.win_ping`](https://docs.ansible.com/ansible/latest/collections/ansible/windows/win_ping_module.html).
#### `win_pip`: Manages Python packages with pip on Windows

```yaml
win_pip:
  name: somepackage
  version: "1.2.3"
  state: present

```

```yaml
win_resources:
  - module: win_pip
    executable: "/path/to/pip"
    extra_args: "--no-cache-dir --force-reinstall"
    name: somepackage
    state: latest

```

For more examples and information on options see
[`c2platform.wincore.win_pip`](https://docs.ansible.com/ansible/latest/collections/c2platform/wincore/win_pip_module.html).
#### `win_power_plan`: Changes the power plan of a Windows system

```yaml
win_power_plan:
  whatever:
    - name: high performance

```

```yaml
win_resources:
  whatever:
    - module: win_power_plan
      guid: 8c5e7fda-e8bf-4a96-9a85-a6e23a8c635c

```

For more examples and information on options see
[`community.windows.win_power_plan`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_power_plan_module.html).
#### `win_powershell`: Run PowerShell scripts
```yaml
win_powershell:
- name: Echo Hello World
  script: |
    echo "Hello World"
```

In this example, the `win_powershell` variable is employed to execute a PowerShell
script with the label "Echo Hello World." The script itself is encapsulated
within the script attribute, allowing you to perform custom actions on Windows
hosts within your Ansible playbook.

#### Install Package Provider using template `install_package_provider.ps.j2`

There is template `install_package_provider.ps.j2` that you can utilize together
with `win_powershell` to install for example **Nuget Package Provider**.

```yaml
win_powershell:
- name: Nuget Package Provider
  script: "{{ lookup('ansible.builtin.template', 'install_package_provider.ps.j2', template_vars={'win_package_provider': 'Nuget'}) }}"
  debug_path: C:\vagrant\logs\nuget-package-provider.yml
```

#### Change Network Adapter Properties using template `net_adapter.ps.j2`.

You can make changes to network adapter properties by utilizing the
`net_adapter.ps.j2` template in conjunction with the `win_powershell` module.
This combination allows you to effectively manage the settings of network
adapters on your system.

For a practical illustration, you can refer to the `groups_vars/ad/main.yml`
file located within the
[RWS Inventory Project](https://c2platform.org/en/docs/gitlab/c2platform/rws/ansible-gis).

```yaml
win_resources:
  ad:
    - name: Don't register connection's addresses in DNS
      type: win_powershell
      script: >-
        {{ lookup('ansible.builtin.template'
        , 'net_adapter.ps.j2'
        , template_vars={'net_adapter': gs_ad_net_adapter }) }}
```

The variable `gs_ad_net_adapter` is defined in the `groups_vars/all/ad.yml` file
as follows:

```yaml
gs_ad_net_adapter:
  name: Ethernet
  property: RegisterThisConnectionsAddress
  value: $false
```

This configuration ensures that the Active Directory (AD) DNS will not create
records for the first adapter that is named, but it will do so for the second
adapter.

For additional information, you can also explore the article
[Setting Up FME Flow with Ansible | C2 Platform](https://c2platform.org/en/docs/howto/rws/arcgis/fme_flow/) and
[Avoid registering unwanted Network Interface Controllers (NICs) in Domain Name System (DNS)](https://learn.microsoft.com/en-us/troubleshoot/windows-server/networking/unwanted-nic-registered-dns-mulithomed-dc).

For more examples and information on options see
[`ansible.windows.win_powershell`](https://docs.ansible.com/ansible/latest/collections/ansible/windows/win_powershell_module.html).
#### `win_psmodule`: Adds or removes a Windows PowerShell module
Use list `win_psmodule` to install or uninstall PowerShell modules. For example
**PowerShell Community Extensions** `Pscx` which is for example necessary for
extracting `tar.gz` tarballs when using `win_unzip`. Some example of such
modules are:

* **Pscx** (PowerShell Community Extensions): Adds a set of additional cmdlets and functions to PowerShell, providing enhanced functionality for tasks like working with the file system, registry, XML, and more.
* **PSWindowsUpdate**: Enables management of Windows updates from PowerShell, allowing you to search for, download, and install Windows updates on target systems.
* **PowerShellGet**: Provides cmdlets for discovering, installing, and managing PowerShell modules from various repositories. It also allows you to work with PowerShell scripts and Desired State Configuration (DSC) resources.
* **PSExcel**: Offers cmdlets for working with Excel files, allowing you to read, write, and manipulate data within Excel spreadsheets using PowerShell.
* **Pester**: A testing framework for PowerShell that allows you to write and run unit tests, integration tests, and other types of tests for your PowerShell code.
* **PSDeploy**: Provides cmdlets for deploying and managing applications and infrastructure using PowerShell. It offers features like deployment rollbacks, parallel deployments, and more.
* **PSLogging**: Helps with logging and generating log files in PowerShell scripts. It includes various logging functions and options for customizing log output.

This `win_psmodules` list uses
[community.windows.win_psmodule](https://docs.ansible.com/ansible/latest/collections/community/windows/win_psmodule_module.html)
to add / remove Powershell modules. To install for example
[Pscx - PowerShell Community Extensions](https://github.com/Pscx/Pscx)

```yaml
win_psmodule:
  - name: Pscx
    force: true
    allow_clobber: true
    state: present
```

For more examples and information on options see
[`community.windows.win_psmodule`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_psmodule_module.html).
#### `win_psmodule_info`: Gather information about PowerShell Modules

```yaml
win_psmodule_info:
  whatever:
    - name: PSReadLine
    - repository: PSGallery

```

```yaml
win_resources:
  whatever:
    - module: win_psmodule_info
      name: PSReadLine
      repository: PSGallery

```

For more examples and information on options see
[`community.windows.win_psmodule_info`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_psmodule_info_module.html).
#### `win_psrepository`: Adds, removes or updates a Windows PowerShell repository.

```yaml
win_psrepository:
  example_repository:
    - name: MyRepository
      source_location: https://myrepo.com
      state: present

```

```yaml
win_resources:
  whatever:
    - module: win_psrepository
      name: AnotherRepository
      source_location: https://anotherrepo.com
      state: present

```

For more examples and information on options see
[`community.windows.win_psrepository`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_psrepository_module.html).
#### `win_psrepository_copy`: Copies registered PSRepositories to other user profiles

```yaml
win_psrepository_copy:
  - name: 'CompanyRepo*'
    exclude: 'CompanyRepo*-Beta'

```

```yaml
win_resources:
  - module: win_psrepository_copy
    name: 'CompanyRepo*'
    exclude: 'CompanyRepo*-Beta'

```

For more examples and information on options see
[`community.windows.win_psrepository_copy`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_psrepository_copy_module.html).
#### `win_psrepository_info`: Gather information about PSRepositories

```yaml
win_psrepository_info:
  example1:
    - name: PSGallery
      package_management_provider: NuGet

```

```yaml
win_resources:
  example2:
    - module: win_psrepository_info
      name: PSGallery

```

For more examples and information on options see
[`community.windows.win_psrepository_info`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_psrepository_info_module.html).
#### `win_psscript`: Install and manage PowerShell scripts from a PSRepository

```yaml
win_psscript:
  whatever:  # optional extra level for dictionary merging
    - name: Test-RPC
      repository: PSGallery
    - name: Get-WindowsAutoPilotInfo
      state: latest

```

```yaml
win_resources:
  whatever:  # optional extra level for dictionary merging
    - module: win_psscript
      allow_prerelease: true
      name: Test-RPC
      state: present

```

For more examples and information on options see
[`community.windows.win_psscript`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_psscript_module.html).
#### `win_psscript_info`: Gather information about installed PowerShell Scripts

```yaml
win_psscript_info:
  whatever:  # optional extra level for dictionary merging
    - name: Test-RPC
    - repository: PSGallery

```

```yaml
win_resources:
  whatever:  # optional extra level for dictionary merging
    - module: win_psscript_info
      name: Test-RPC
      repository: PSGallery

```

For more examples and information on options see
[`community.windows.win_psscript_info`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_psscript_info_module.html).
#### `win_pssession_configuration`: Manage PSSession Configurations

```yaml
win_pssession_configuration:
  - name: WebAdmin
    modules_to_import:
      - WebAdministration
      - IISAdministration
    description: This endpoint has IIS modules pre-loaded

```

```yaml
win_resources:
  - module: win_pssession_configuration
    name: GloboCorp.Admin
    company_name: Globo Corp
    description: Admin Endpoint
    execution_policy: restricted

```

For more examples and information on options see
[`community.windows.win_pssession_configuration`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_pssession_configuration_module.html).
#### `win_rds_cap`: Manage Connection Authorization Policies (CAP) on a Remote Desktop Gateway server

```yaml
win_rds_cap:
  My_CAP:
    - name: My CAP
      user_groups:
        - BUILTIN\users
      session_timeout: 30
      session_timeout_action: disconnect
      allow_only_sdrts_servers: yes
      redirect_clipboard: yes
      redirect_drives: no
      redirect_printers: no
      redirect_serial: no
      redirect_pnp: no
      state: enabled

```

```yaml
win_resources:
  whatever:
    - module: win_rds_cap
      name: Another CAP
      user_groups:
        - Administrators
      session_timeout: 60
      state: present

```

For more examples and information on options see
[`community.windows.win_rds_cap`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_rds_cap_module.html).
#### `win_rds_rap`: Manage Resource Authorization Policies (RAP) on a Remote Desktop Gateway server

```yaml
win_rds_rap:
  - name: My RAP
    description: Allow all users to connect to any resource through ports 3389 and 3390
    user_groups:
      - BUILTIN\users
    computer_group_type: allow_any
    allowed_ports:
      - 3389
      - 3390
    state: enabled

```

```yaml
win_resources:
  - module: win_rds_rap
    name: My RAP
    description: Allow all users to connect to any resource through ports 3389 and 3390
    user_groups:
      - BUILTIN\users
    computer_group_type: allow_any
    allowed_ports:
      - 3389
      - 3390

```

For more examples and information on options see
[`community.windows.win_rds_rap`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_rds_rap_module.html).
#### `win_rds_settings`: Manage main settings of a Remote Desktop Gateway server

```yaml
win_rds_settings:
  - certificate_hash: "B0D0FA8408FC67B230338FCA584D03792DA73F4C"
    max_connections: 50
    ssl_bridging: "https_https"

```

```yaml
win_resources:
  - module: win_rds_settings
    certificate_hash: "B0D0FA8408FC67B230338FCA584D03792DA73F4C"
    max_connections: 100
    enable_only_messaging_capable_clients: true

```

For more examples and information on options see
[`community.windows.win_rds_settings`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_rds_settings_module.html).
#### `win_reboot`: Reboot a windows machine
You can use the `win_reboot` variable to manage server reboots efficiently by
leveraging the
[ansible.windows.win_reboot](https://docs.ansible.com/ansible/latest/collections/ansible/windows/win_reboot_module.html)
module. This versatile variable not only supports the parameters of the module itself but also offers additional attribute `tags`. This parameter allows you to assign tags to the job, making it easier to manage and categorize tasks. In the provided example, we've assigned the 'setup' tag to indicate that this reboot is part of the initial system setup process.

Typically, the win_reboot variable is employed when configuring a new system.
For a practical demonstration, refer to the `groups_vars/windows/main.yml` file
within the
[RWS Inventory Project](https://c2platform.org/en/docs/gitlab/c2platform/rws/ansible-gis).

Here's an example YAML snippet illustrating the use of the `win_reboot` variable:

```yaml
win_reboot:
  - name: Reboot new servers
    type: win_reboot
    msg-log: >-
      Rebooting the server as part of the system setup process.
      This is required to apply initial configurations and
      ensure a fully functional system.
    reboot_timeout: 600
    tags: ['setup']
```

In this example, we specify the `name` to provide a clear description of the
reboot task. The `msg-log` field allows you to include a message explaining the
purpose of the reboot, ensuring that anyone reviewing the Ansible log
understands its significance. Finally, we set a `reboot_timeout` to 600 seconds,
indicating the maximum time allowed for the reboot to complete successfully.

For more examples and information on options see
[`ansible.windows.win_reboot`](https://docs.ansible.com/ansible/latest/collections/ansible/windows/win_reboot_module.html).
#### `win_reg_stat`: Get information about Windows registry keys

```yaml
win_reg_stat:
  - name: CommonFilesDir
    path: HKLM:\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion

```

```yaml
win_resources:
  - module: win_reg_stat
    name: ''
    path: HKLM:\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion

```

For more examples and information on options see
[`ansible.windows.win_reg_stat`](https://docs.ansible.com/ansible/latest/collections/ansible/windows/win_reg_stat_module.html).
#### `win_regedit`: Add, change, or remove registry keys and values
The `win_regedit` list empowers you to effortlessly add, modify, or remove
registry keys and values on Windows systems. This module leverages the
capabilities of the Ansible module
[`ansible.windows.win_regedit`](https://docs.ansible.com/ansible/latest/collections/ansible/windows/win_regedit_module.html).

One practical application of this module is configuring a `proxy server`. For an
illustrative example of how `win_regedit` is employed to set up a proxy server,
you can refer to the `groups_vars/windows/main.yml` file within the
[RWS Inventory Project](https://c2platform.org/en/docs/gitlab/c2platform/rws/ansible-gis).
This project showcases a real-world scenario where win_regedit plays a pivotal
role in configuring proxy server settings on Windows systems.

For more examples and information on options see
[`ansible.windows.win_regedit`](https://docs.ansible.com/ansible/latest/collections/ansible/windows/win_regedit_module.html).
#### `win_region`: Set the region and format settings

```yaml
win_region:
  - format: en-US

```

```yaml
win_resources:
  - module: win_region
    format: en-US

```

For more examples and information on options see
[`community.windows.win_region`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_region_module.html).
#### `win_regmerge`: Merges the contents of a registry file into the Windows registry

```yaml
win_regmerge:
  whatever:  # optional extra level for dictionary merging
    - compare_key: HKLM:\SOFTWARE\MyCompany
      path: C:\autodeploy\myCompany-settings.reg

```

```yaml
win_resources:
  whatever:  # optional extra level for dictionary merging
    - module: win_regmerge
      compare_key: HKLM:\SOFTWARE\MyCompany
      path: C:\autodeploy\myCompany-settings.reg

```

For more examples and information on options see
[`community.windows.win_regmerge`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_regmerge_module.html).
#### `win_robocopy`: Synchronizes the contents of two directories using Robocopy

```yaml
win_robocopy:
  whatever:
    - src: C:\\DirectoryOne
      dest: C:\\DirectoryTwo

```

```yaml
win_resources:
  whatever:
    - module: win_robocopy
      src: C:\\DirectoryOne
      dest: C:\\DirectoryTwo
      recurse: true

```

For more examples and information on options see
[`community.windows.win_robocopy`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_robocopy_module.html).
#### `win_route`: Add or remove a static route

```yaml
win_route:
  whatever:
    - destination: 192.168.2.10/32
      gateway: 192.168.1.1
      metric: 1
      state: present

```

```yaml
win_resources:
  whatever:
    - module: win_route
      destination: 192.168.2.10/32
      gateway: 192.168.1.1
      metric: 1
      state: present

```

For more examples and information on options see
[`community.windows.win_route`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_route_module.html).
#### `win_say`: Text to speech module for Windows to speak messages and optionally play sounds

```yaml
win_say:
  whatever:
    - msg: "Warning, deployment commencing in 5 minutes, please log out."

```

```yaml
win_resources:
  whatever:
    - module: win_say
      msg: "New software installed"

```

For more examples and information on options see
[`community.windows.win_say`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_say_module.html).
#### `win_scheduled_task`: Manage scheduled tasks
```yaml
win_scheduled_task:
  - name: cmd-task
    description: Command prompt
    actions:
      - path: cmd.exe
        arguments: /c hostname
      - path: cmd.exe
        arguments: /c whoami
    triggers:
      - type: daily
        start_boundary: '2017-10-09T09:00:00'
    username: SYSTEM
    state: present
    enabled: yes
  - name: powershell-task
    description: Run a PowerShell script
    actions:
      - path: C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe
        arguments: -ExecutionPolicy Unrestricted -NonInteractive -File C:\winapp\helloworld.ps1
    triggers:
      - type: boot
        username: NETWORK SERVICE
    run_level: highest
    state: present
```

For more examples and information on options see
[`community.windows.win_scheduled_task`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_scheduled_task_module.html).
#### `win_scheduled_task_stat`: Get information about Windows Scheduled Tasks

```yaml
win_scheduled_task_stat:
  whatever:  # optional extra level for dictionary merging
    - name: task name
      path: folder path

```

```yaml
win_resources:
  whatever:  # optional extra level for dictionary merging
    - module: win_scheduled_task_stat
      name: task name
      path: folder path

```

For more examples and information on options see
[`community.windows.win_scheduled_task_stat`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_scheduled_task_stat_module.html).
#### `win_scoop`: Manage packages using Scoop

```yaml
win_scoop:
  whatever:  # optional extra level for dictionary merging
    - name: jq

```

```yaml
win_resources:
  whatever:  # optional extra level for dictionary merging
    - module: win_scoop
      name: curl
      state: present

```

For more examples and information on options see
[`community.windows.win_scoop`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_scoop_module.html).
#### `win_scoop_bucket`: Manage Scoop buckets

```yaml
win_scoop_bucket:
  whatever:
    - name: extras

```

```yaml
win_resources:
  whatever:
    - module: win_scoop_bucket
      name: versions
      state: absent

```

For more examples and information on options see
[`community.windows.win_scoop_bucket`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_scoop_bucket_module.html).
#### `win_security_policy`: Change local security policy settings

```yaml
win_security_policy:
  whatever:
    - section: System Access
      key: NewGuestName
      value: Guest Account

```

```yaml
win_resources:
  whatever:
    - module: win_security_policy
      section: System Access
      key: MaximumPasswordAge
      value: 15

```

For more examples and information on options see
[`community.windows.win_security_policy`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_security_policy_module.html).
#### `win_service`: Manage and query Windows services

```yaml
win_service:
  - name: spooler
    state: restarted

```

```yaml
win_resources:
  whatever:
    - module: win_service
      name: service_name
      path: C:\\temp\\test.exe

```

For more examples and information on options see
[`ansible.windows.win_service`](https://docs.ansible.com/ansible/latest/collections/ansible/windows/win_service_module.html).
#### `win_service_info`: Gather information about Windows services

```yaml
win_service_info:
  - name: WinRM

```

```yaml
win_resources:
  - module: win_service_info
    name: WinRM

```

For more examples and information on options see
[`ansible.windows.win_service_info`](https://docs.ansible.com/ansible/latest/collections/ansible/windows/win_service_info_module.html).
#### `win_share`: Manage Windows shares

```yaml
win_share:
  secret_share:
    - name: internal
      description: top secret share
      path: C:\\shares\\internal
      list: false
      full: Administrators,CEO
      read: HR-Global
      deny: HR-External

```

```yaml
win_resources:
  specific_share:
    - module: win_share
      name: company
      description: public company share
      path: C:\\shares\\company
      list: true
      full: Administrators,CEO
      read: Global

```

For more examples and information on options see
[`ansible.windows.win_share`](https://docs.ansible.com/ansible/latest/collections/ansible/windows/win_share_module.html).
#### `win_shell`: Execute shell commands on target hosts
```yaml
win_shell:
  - name: "Ensure the required NuGet package provider version is installed"
    win_shell: Find-PackageProvider -Name Nuget -ForceBootstrap -IncludeDependencies -Force
    changed_when: false  # It's not possible to detect changes
```

For more examples and information on options see
[`ansible.windows.win_shell`](https://docs.ansible.com/ansible/latest/collections/ansible/windows/win_shell_module.html).
#### `win_shortcut`: Manage shortcuts on Windows
See for example
[group_vars/gs_portal/shortcuts.yml](https://gitlab.com/c2platform/rws/ansible-gis/-/blob/master/group_vars/gs_portal/shortcuts.yml)
in [ansible-gis](https://gitlab.com/c2platform/rws/ansible-gis/) project.

```yaml
win_shortcut:
  - src: https://c2platform.org/en/docs/howto/rws/arcgis/portal/
    dest: '%USERPROFILE%\Desktop\Setup ArcGIS Portal and ArcGIS Web Adaptors using Ansible.url'
  - src: https://gsd-agportal1.internal.c2platform.org/server/manager/
    dest: '%USERPROFILE%\Desktop\ArcGIS Server Manager.url'
  - src: https://gsd-agportal1.internal.c2platform.org/server/admin/
    dest: '%USERPROFILE%\Desktop\ArcGIS Server Administrator Directory.url'
```

For more examples and information on options see
[`community.windows.win_shortcut`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_shortcut_module.html).
#### `win_snmp`: Configures the Windows SNMP service

```yaml
win_snmp:
  - action: set
    community_strings:
      - public
      - snmp-ro
    permitted_managers:
      - 192.168.1.1
      - 192.168.1.2

```

```yaml
win_resources:
  - module: win_snmp
    action: add
    community_strings:
      - public
      - private
    permitted_managers:
      - 192.168.1.1
      - 192.168.1.2

```

For more examples and information on options see
[`community.windows.win_snmp`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_snmp_module.html).
#### `win_tempfile`: Creates temporary files and directories

```yaml
win_tempfile:
  whatever:
    - state: directory
      suffix: build

```

```yaml
win_resources:
  whatever:
    - module: win_tempfile
      path: "C:\\Temp\\"
      prefix: "test"

```

For more examples and information on options see
[`ansible.windows.win_tempfile`](https://docs.ansible.com/ansible/latest/collections/ansible/windows/win_tempfile_module.html).
#### `win_template`: Template a file out to a remote server

```yaml
win_template:
  whatever:
    - src: /mytemplates/file.conf.j2
      dest: C:\\Temp\\file.conf

```

```yaml
win_resources:
  whatever:
    - module: win_template
      src: unix/config.conf.j2
      dest: C:\\share\\unix\\config.conf
      newline_sequence: '\\n'
      backup: true

```

For more examples and information on options see
[`ansible.windows.win_template`](https://docs.ansible.com/ansible/latest/collections/ansible/windows/win_template_module.html).
#### `win_timezone`: Sets Windows machine timezone

```yaml
win_timezone:
  whatever:
    - timezone: "Romance Standard Time"

```

```yaml
win_resources:
  whatever:
    - module: win_timezone
      timezone: "Central Standard Time"

```

For more examples and information on options see
[`community.windows.win_timezone`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_timezone_module.html).
#### `win_toast`: Sends Toast windows notification to logged in users on Windows 10 or later hosts

```yaml
win_toast:
  whatever:
    - expire: 60
      title: System Upgrade Notification
      msg: Automated upgrade about to start. Please save your work and log off before {{ deployment_start_time }}

```

```yaml
win_resources:
  whatever:
    - module: win_toast
      expire: 45
      group: Powershell
      msg: Hello, World!
      popup: true
      tag: Ansible
      title: Notification HH:mm

```

For more examples and information on options see
[`community.windows.win_toast`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_toast_module.html).
#### `win_unzip`: Unzips compressed files and archives on the Windows node

```yaml
win_unzip:
  example1:
    - src: C:\\Users\\Phil\\Logs.bz2
      dest: C:\\Users\\Phil\\OldLogs
      creates: C:\\Users\\Phil\\OldLogs
    - src: C:\\Logs\\application-error-logs.gz
      dest: C:\\ExtractedLogs\\application-error-logs
  example2:
    - src: C:\\Downloads\\ApplicationLogs.zip
      dest: C:\\Application\\Logs
      recurse: yes
      delete_archive: yes

```

```yaml
win_resources:
  whatever:
    - module: win_unzip
      src: C:\\LibraryToUnzip.zip
      dest: C:\\Lib
      remove: yes
    - module: win_unzip
      src: C:\\Downloads\\ApplicationLogs.7z
      dest: C:\\Application\\Logs
      password: abcd
      delete_archive: yes

```

For more examples and information on options see
[`community.windows.win_unzip`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_unzip_module.html).
#### `win_updates`: Download and install Windows updates

```yaml
win_updates:
  - category_names: '*'
    reboot: true

```

```yaml
win_resources:
  - module: win_updates
    category_names:
      - SecurityUpdates
      - CriticalUpdates
      - UpdateRollups
    reboot: true

```

For more examples and information on options see
[`ansible.windows.win_updates`](https://docs.ansible.com/ansible/latest/collections/ansible/windows/win_updates_module.html).
#### `win_uri`: Interacts with webservices

```yaml
win_uri:
  - name: Perform a GET and Store Output
    ansible.windows.win_uri:
      url: http://example.com/endpoint
    register: http_output

```

```yaml
win_resources:
  - module: win_uri
    url: http://example.com/
    method: GET
    headers:
      host: www.somesite.com

```

For more examples and information on options see
[`ansible.windows.win_uri`](https://docs.ansible.com/ansible/latest/collections/ansible/windows/win_uri_module.html).
#### `win_user`: Manages local Windows user accounts
```yaml
win_user:
  - name: sa_user
    description: Service account user
    fullname: sa_user
    groups: Administrators
    password: supersecret # Vault
    password_expired: no
```

For more examples and information on options see
[`ansible.windows.win_user`](https://docs.ansible.com/ansible/latest/collections/ansible/windows/win_user_module.html).
#### `win_user_profile`: Manages the Windows user profiles.

```yaml
win_user_profile:
  - username: ansible-account
    state: present

```

```yaml
win_resources:
  - module: win_user_profile
    name: ansible
    state: present

```

For more examples and information on options see
[`community.windows.win_user_profile`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_user_profile_module.html).
#### `win_user_right`: Manage Windows User Rights
For example to grant logon as a service permission:

```yaml
win_user_right:
  - name: SeServiceLogonRight
    users: gis-backup-operator
```

For more examples and information on options see
[`ansible.windows.win_user_right`](https://docs.ansible.com/ansible/latest/collections/ansible/windows/win_user_right_module.html).
#### `win_wait_for`: Waits for a condition before continuing

```yaml
win_wait_for:
  - name: Wait until process complete is in the file before continuing
    path: C:\temp\log.txt
    regex: process complete

```

```yaml
win_resources:
  - module: win_wait_for
    path: C:\temp\log.txt
    regex: process complete

```

For more examples and information on options see
[`ansible.windows.win_wait_for`](https://docs.ansible.com/ansible/latest/collections/ansible/windows/win_wait_for_module.html).
#### `win_wait_for_process`: Waits for a process to exist or not exist before continuing.

```yaml
win_wait_for_process:
  - process_name_pattern: 'v(irtual)?box(headless|svc)?'
    state: absent
    timeout: 500

```

```yaml
win_resources:
  - module: win_wait_for_process
    process_name_exact: cmd
    state: present
    timeout: 500
    sleep: 5
    process_min_count: 3

```

For more examples and information on options see
[`community.windows.win_wait_for_process`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_wait_for_process_module.html).
#### `win_wakeonlan`: Send a magic Wake-on-LAN (WoL) broadcast packet

```yaml
win_wakeonlan:
  - mac: 00:00:5E:00:53:66
    broadcast: 192.0.2.23


```

```yaml
win_resources:
  - module: win_wakeonlan
    mac: 00:00:5E:00:53:66
    broadcast: 192.0.2.23

```

For more examples and information on options see
[`community.windows.win_wakeonlan`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_wakeonlan_module.html).
#### `win_webpicmd`: Installs packages using Web Platform Installer command-line

```yaml
win_webpicmd:
  whatever:  # optional extra level for dictionary merging
    - name: URLRewrite2

```

```yaml
win_resources:
  whatever:  # optional extra level for dictionary merging
    - module: win_webpicmd
      name: URLRewrite2

```

For more examples and information on options see
[`community.windows.win_webpicmd`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_webpicmd_module.html).
#### `win_whoami`: Get information about the current user and process

```yaml
win_whoami:
  whatever:
    - impersonation_level: "SecurityAnonymous"
      login_domain: "DOMAIN"
    - token_type: "TokenPrimary"
      upn: "Administrator@DOMAIN.COM"

```

```yaml
win_resources:
  whatever:
    - module: win_whoami
      impersonation_level: "SecurityAnonymous"
      login_domain: "DOMAIN"

```

For more examples and information on options see
[`ansible.windows.win_whoami`](https://docs.ansible.com/ansible/latest/collections/ansible/windows/win_whoami_module.html).
#### `win_xml`: Manages XML file content on Windows hosts

```yaml
win_xml:
  whatever:
    - path: C:\\apache-tomcat\\webapps\\myapp\\WEB-INF\\web.xml
      fragment: '<filter><filter-name>MyFilter</filter-name><filter-class>com.example.MyFilter</filter-class></filter>'
      xpath: '/*'

```

```yaml
win_resources:
  whatever:
    - module: win_xml
      path: C:\\Tomcat\\conf\\server.xml
      xpath: '//Server/Service[@name="Catalina"]/Connector[@port="9443"]'
      attribute: 'sslEnabledProtocols'
      fragment: 'TLSv1,TLSv1.1,TLSv1.2'
      type: attribute

```

For more examples and information on options see
[`community.windows.win_xml`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_xml_module.html).
#### `win_zip`: Compress file or directory as zip archive on the Windows node

```yaml
win_zip:
  whatever:  # optional extra level for dictionary merging
    - src: C:\Users\hiyoko\log.txt
      dest: C:\Users\hiyoko\log.zip
    - src: C:\Users\hiyoko\log
      dest: C:\Users\hiyoko\log.zip

```

```yaml
win_resources:
  whatever:  # optional extra level for dictionary merging
    - module: win_zip
      src: C:\Users\hiyoko\log.txt
      dest: C:\Users\hiyoko\log.zip

```

For more examples and information on options see
[`community.windows.win_zip`](https://docs.ansible.com/ansible/latest/collections/community/windows/win_zip_module.html).
<!-- end supported_modules -->



## Dependencies

<!--   A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles. -->

## Example Playbook

<!--   Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too: -->

To find practical examples demonstrating the utilization of the
`c2platform.wincore.win` Ansible role, please consult the
`groups_vars/windows/main.yml` file located within the
[RWS Inventory Project](https://c2platform.org/en/docs/gitlab/c2platform/rws/ansible-gis).
This file provides insightful illustrations of how to employ the mentioned
Ansible role.
