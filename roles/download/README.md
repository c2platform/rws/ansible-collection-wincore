# Ansible Role c2platform.wincore.downloads

This Ansible role simplifies the management of file downloads, which are often necessary for the installation of various products. It can be included in other roles using `ansible.builtin.include_role`.

- [Requirements](#requirements)
- [Role Variables](#role-variables)
  - [`download_temp_dir`](#download_temp_dir)
  - [`download_files`](#download_files)
  - [`download_proxy`](#download_proxy)
  - [Downloads to Shared Storage](#downloads-to-shared-storage)
- [Dependencies](#dependencies)
- [Example Playbook](#example-playbook)

## Requirements

<!-- Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required. -->

## Role Variables

<!--  A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well. -->

### `download_temp_dir`

The `download_temp_dir` variable allows you to specify a temporary folder where
files and binaries will be downloaded. You can customize its behavior with the
following parameters:

* `path` (default: `D:\temp`): Path to the folder.
* `create` (default: `yes`): Create the folder if it doesn't exist.
* `recursive` (default: `true`): Create the folder, including intermediate folders.
* `delete` (default: `no`): Delete the folder after processing (part of `cleanup.yml` tasks).
* `delete_files` (default: `true`): Delete files after processing (part of `cleanup.yml` tasks).

Example configuration:

```yaml
download_temp_dir:
  path: D:\temp
  create: yes
  delete: no
  recursive: true
  delete_files: true
```

### `download_files`

The download_files variable allows you to specify files to be downloaded using
the `ansible.community.win_get_url` module. Files will be downloaded to the directory configured
using `download_temp_dir`. This variable is a dictionary of dictionaries,
allowing you to configure multiple downloads. Each download can have an optional
`enabled` parameter to enable or disable it.

In addition to the parameters supported by the `ansible.community.win_get_url`
module, you can use the following parameters to control downloads:

* `enabled` (default: `yes`): enable or disable a download.
* `unzip` (default: `no`): extract zip file after download.

Example configuration:

```yaml
download_files:
    portal:
      url: http://example.com/software/arcgis/11.1/PortalForArcGIS.zip
    webstyles:
      url: http://example.com/software/arcgis/11.1/ArcGISWebStyles.zip
      enabled: yes
    license:
      url: http://example.com/software/arcgis/11.1/portal.json
```

### `download_proxy`

To configure a proxy server, you can employ the `download_proxy` dictionary. These
settings will be applicable to all downloads configured using `download_files`.

Here's how you can set up the proxy in YAML:

```yaml
download_proxy:
  url: http://proxy.example.com:8080
  username: myproxyuser
  password: myproxypw
  enabled: true
```

These settings will be passed as parameters to the `ansible.windows.win_get_url`
module, utilizing the following parameters:

* `proxy_url`
* `proxy_username`
* `proxy_password`
* `proxy_use_default_credential`
* `use_proxy`

If you wish to customize the proxy settings for specific downloads, you can
override the `download_proxy` variable within the `download_files` dictionary,
like so:

```yaml
download_files:
    portal:
      url: http://example.com/software/arcgis/11.1/PortalForArcGIS.zip
      proxy_url: http://proxy.example.com:8080
      proxy_username: myproxyuser
      proxy_password: myproxypw
      use_proxy: true
    webstyles:
      url: http://example.com/software/arcgis/11.1/ArcGISWebStyles.zip
    license:
      url: http://example.com/software/arcgis/11.1/portal.json
```

Alternatively, if you prefer to have the `download_proxy` configured by disabled
by default and enable it for specific downloads, you can set it up as follows:

```yaml
download_proxy:
  url: http://proxy.example.com:8080
  username: myproxyuser
  password: myproxypw
  enabled: false
```

Then, enable the proxy on selected downloads, like the portal download:

```yaml
download_files:
    portal:
      url: http://example.com/software/arcgis/11.1/PortalForArcGIS.zip
      use_proxy: true
    webstyles:
      url: http://example.com/software/arcgis/11.1/ArcGISWebStyles.zip
    license:
      url: http://example.com/software/arcgis/11.1/portal.json
```

This flexible configuration allows you to manage downloads and proxy settings
with precision.

### Downloads to Shared Storage

This Ansible role is specifically designed to facilitate efficient downloads to
shared storage systems, including CIFS shares. It utilizes the
`c2platform.wincore.win_download_lock` module to establish a unique lock file
for every download task. This critical feature prevents the occurrence of
multiple hosts trying to download and process the identical file concurrently.

By implementing this locking mechanism, the role guarantees that only the host
which manages to successfully generate the lock file will carry out the download
process. This approach not only serializes access to the shared resources,
ensuring orderly and conflict-free downloads but also significantly reduces
unnecessary network traffic by avoiding duplicate downloads across a multi-host,
multi-environment setup.

## Dependencies

<!--   A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles. -->

## Example Playbook

<!--   Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too: -->

You can use this Ansible role in a playbook as demonstrated below:

```yaml
- hosts: servers
  roles:
      - { role: c2platform.wincore.download }

  vars:
    download_files:
      portal:
        url: http://example.com/software/arcgis/11.1/PortalForArcGIS.zip
```

However, this role is primarily designed to be included within other roles, as
illustrated in the following example. To integrate this role into your Ansible
project, create a file named `tasks/download.yml` within your role directory, and
define the following tasks:

```yaml
- name: Include role download
  ansible.builtin.include_role:
    name: c2platform.wincore.download
    tasks_from: "{{ download_tasks_from | default('main') }}"
  vars:
    download_files: "{{ tomcat_versions[tomcat_version] }}"
    download_temp_dir: "{{ tomcat_temp_dir }}"
```

Make sure to include these download tasks at the appropriate location within
your main playbook:

```yaml
- name: Include download tasks
  ansible.builtin.include_tasks: download.yml
```

For cleanup of download files, include the following tasks at the appropriate
location within your play:

```yaml
- name: Include download cleanup tasks
  ansible.builtin.include_tasks: download.yml
  vars:
    download_tasks_from: cleanup
```

For a comprehensive example of how to use this role, you can refer to the
`arcgis_portal` role in the
[`c2platform.gis`](https://gitlab.com/c2platform/rws/ansible-collection-gis)
collection.

This structure allows you to modularize your Ansible project effectively and maintain a clean separation of concerns when dealing with download tasks.
