# Ansible Collection - c2platform.wincore

[![Pipeline Status](https://gitlab.com/c2platform/rws/ansible-collection-wincore/badges/master/pipeline.svg?style=flat-square&key_text=Pipeline+Status&key_width=90)](https://gitlab.com/c2platform/rws/ansible-collection-wincore/-/commits/master)
[![Latest Release](https://gitlab.com/c2platform/rws/ansible-collection-wincore/-/badges/release.svg?style=flat-square)](https://gitlab.com/c2platform/rws/ansible-collection-wincore/-/releases)
[![Ansible Galaxy](https://img.shields.io/badge/Galaxy-_c2platform.wincore-blue.svg)](https://galaxy.ansible.com/ui/repo/published/c2platform/wincore/)

C2 Platform generic roles for MS Windows that are used by all or some other
roles. These roles typically don't create services / processes on target node
but are dependencies e.g. packages required by those roles. Or these roles help
with Ansible provisioning for example offers generic Ansible modules, filters
etc.

## Roles

* [`win`](./roles/win) Manage MS Windows systems using more than 117+ Ansible
modules from the `ansible.windows`, `community.windows` and
`chocolatey.chocolatey` collection.
* [`download`](./roles/download) manage downloads.
* [`ad`](./roles/ad) Active Directory ( AD) management using
  [`microsoft.ad`](https://docs.ansible.com/ansible/latest/collections/microsoft/ad/)
  Ansible collection.

## Modules and Filters

For detailed information on the available modules and filters within this
collection, please refer to the
[Ansible Galaxy](https://galaxy.ansible.com/ui/repo/published/c2platform/wincore/docs/)
page.

You can of course also use the `ansible-doc` command to explore the
documentation:

```bash
ansible-doc -t module --list c2platform.wincore
ansible-doc -t filter --list c2platform.wincore
ansible-doc -t filter <FILTER_NAME>
ansible-doc -t module <MODULE_NAME>
```

