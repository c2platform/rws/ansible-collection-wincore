# Ansible Collection - c2platform.wincore

[![Pipeline Status](https://gitlab.com/c2platform/rws/ansible-collection-wincore/badges/master/pipeline.svg?style=flat-square&key_text=Pipeline+Status&key_width=90)](https://gitlab.com/c2platform/rws/ansible-collection-wincore/-/commits/master) [![Latest Release](https://gitlab.com/c2platform/rws/ansible-collection-wincore/-/badges/release.svg?style=flat-square)](https://gitlab.com/c2platform/rws/ansible-collection-wincore/-/releases)
[![Ansible Galaxy](https://img.shields.io/badge/Galaxy-_c2platform.wincore-blue.svg)](https://galaxy.ansible.com/ui/repo/published/c2platform/wincore/)

See full [README](https://gitlab.com/c2platform/rws/ansible-collection-wincore/-/blob/master/README.md).
