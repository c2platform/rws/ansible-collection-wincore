# CHANGELOG

## 1.0.16 ( )

* [`win`](./roles/win/) added `delegate_to` to all tasks.
* [`win`](./roles/win/) fix `win_ping` `data` optional.

## 1.0.15 ( 2024-12-19 )

* `c2platform.core` → `1.0.24`

## 1.0.14 ( 2024-11-14 )

* [`win`](./roles/win/) added tags `install`, `system`, `win`.
* [`download`](./roles/download/) added tags `install`, `application`,
  `download`.
* [`ad`](./roles/ad/) added tags `install`, `system`, `ad`, `windows`.
* [`win`](./roles/win/) Simplified HTTPS Configuration for IIS: Added support to
    configure IIS HTTPS bindings using `friendly_name` in addition to
    `certificate_hash`, making certificate management more intuitive. Extended
    the `win_certificate` module with a new `friendly_name` option for easier
    certificate identification. Introduced a new filter
    `c2platform.wincore.win_iis_webbinding_cert_hash` to dynamically resolve
    certificate thumbprints from either their hash or friendly name, seamlessly
    integrating with the `win_iis_webbinding` Ansible task.
* [`win`](./roles/win/) fix `win_resource_groups_disabled` and improved `label`.
* [`win`](./roles/win/) fix bug in `win_mapped_drive`.

## 1.0.13 ( 2024-06-26 )

* `c2platform.core` → `1.0.22`
* Dependency `c2platform.dev.vagrant_hosts` → `c2platform.core.vagrant_hosts`.
* [`win`](./roles/win/) added `win_certificate`.

## 1.0.12 ( 2024-05-07 )

* [`win`](./roles/win/) new variable `win_roles`.

## 1.0.11 ( 2024-05-02 )

* [`win`](./roles/win/) fix bug in processing of `win_<module>` variables.
* [`win`](./roles/win/) fix name `win_package`.
* [`win`](./roles/win/) fix tag `win_net_adapter_feature`.
* [`win`](./roles/win/) added `win_include_role`.
* [`win`](./roles/win/) fix bug in `win_resources_types` using new filter.
  `c2platform.core.role_variable` (from `c2platform.core` `1.0.17`).
* [`win`](./roles/win/) improved doc.
* [`win`](./roles/win/) added module `ansible.builtin.fail`.

## 1.0.10 ( 2024-04-25)

* Upgrade to `c2platform.core` `1.0.16`.
* Fix filter `c2platform.wincore.download_extract_folder`.
* [`win`](./roles/win/) added extra selected modules from `ansible.windows`,
  `community.windows` and `chocolatey.chocolatey`.
* [`win`](./roles/win/) fix `win_mapped_drive` and `win_share`:
  `win_resource_item` → `item`.

## 1.0.9 ( 2024-03-12 )

* Added missing role and filter documentation.

## 1.0.8 ( 2024-03-12 )

* Added missing role and filter documentation.

## 1.0.7 ( 2024-03-08 )

* [`win`](./roles/win/) added a new module `c2platform.wincore.win_pip` and
  associated variable `win_pip`.
* [`win`](./roles/win/) added new variable / module `win_wait_for`.

## 1.0.6 ( 2024-02-29 )

* [`download`](./roles/download/) support for downloading to shared storage using
  `win_download_lock` module.
* [`win`](./roles/win/) added `win_credential`. Added `ansible_become` etc vars
  for privilege escalation.
* [`download`](./roles/download/) new vars `download_ansible_become` etc for
  privilege escalation.
* [`download`](./roles/download/) to shared storage using lock file using
  `win_download_lock` module.

## 1.0.5 ( 2024-01-31 )

* [`win`](./roles/win/) added `win_xml`, `win_lineinfile`.
* [`win`](./roles/win/) added `module` as an alias for `type`.
* [`win`](./roles/win/) using filter `c2platform.core.resources` for `with_items`.
* [`win`](./roles/win/) added `win_path`.
* [`win`](./roles/win/) added `win_environment`.
* Module [`c2platform.wincore.win_certificate`](./plugins/modules/win_certificate.ps1)
  added attribute `tmp_file_path` with default value `C:\Temp\ExportedCertificate.cer`.
* [`download`](./roles/download/) check stat and skipping of download and extract
  folders. Added filters `download_extract_folder` and `download_file_path`.
* [`win`](./roles/win/) added `tags` attribute on all resources.
* [`win`](./roles/win/) added `when` attribute on all resources.
* [`win`](./roles/win/) added template `net_adapter.ps.j2`.
* [`ad`](./roles/ad/) new role for the management of Microsoft Active Directory
  (AD). With `ad_domain`, `ad_domain_controller`, `ad_membership` and `ad_user`.
* [`download`](./roles/download/) `create` key of `download_temp_dir` optional,
  default `true`.
* [`win`](./roles/win/) `resources` attribute optional for all types.
* [`win`](./roles/win/) added `win_share`, `win_mapped_drive`.
* [`win`](./roles/win/) added template `install_package_provider.ps.j2`.
* [`win`](./roles/win/) added `win_reboot`.
* [`win`](./roles/win/) added `win_regedit`, `win_shell`.
* [`win`](./roles/win/) added `win_proxy_password`, `win_proxy_url` and
  `win_proxy_username`. Currently only utilized by `win_chocolatey`.

## 1.0.4 ( 2023-09-27)

* [`download`](./roles/download/) added `download_proxy` dictionary.
* [`download`](./roles/download/) readme update.

## 1.0.3 ( 2023-09-14 )

* [`win`](./roles/win) fixed issues with `win_acl`, `win_copy` and `win_group_membership`.
* Updated dependency `c2platform.core` `>=1.0.2`.

## 1.0.2 ( 2023-09-13 )

* [`win`](./roles/win) added `win_hosts`.
* [`win`](./roles/win) completely refactored and new vars `win_resources` and
  `win_resources_types`.

## 1.0.1 ( 2023-09-07)

* [`win`](./roles/win) added `win_firewalls`.
* [`win`](./roles/win) added `win_shortcuts`.
* Module [`c2platform.wincore.win_certificate`](./plugins/modules/win_certificate.ps1)
* Module [`c2platform.wincore.win_package_facts`](./plugins/modules/win_package_facts.ps1)

## 1.0.0 ( 2023-06-16 )

* [`win`](./roles/win) added `win_psmodules`.
* `ps` role removed, part of [`win`](./roles/win).

## 0.1.1 ( 2023-06-14 )

* [`win`](./roles/win) add / remove PS modules using list `win_psmodules`.
* [`win`](./roles/win) add / remove PS modules using list `win_psmodules` and
  manage group memberships using `win_group_membership`.

## 0.1.0 ( 2023-06-08 )

* [`win`](./roles/win) initial role for MS Windows.
* `ps` initial role for managing PowerShell modules.
